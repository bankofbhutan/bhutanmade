<?php
/**
 * @Author      : Kien
 * @package     Marketplace_Seller_Membership
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\SellerMembership\Controller\Adminhtml;

class Membership extends MembershipActions
{
	/**
	 * Form session key
	 * @var string
	 */
    protected $_formSessionKey  = 'magetop_membership_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey      = 'Magetop_SellerMembership::membership';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass      = 'Magetop\SellerMembership\Model\Membership';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu      = 'Magetop_SellerMembership::membership';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField     = 'status';
}