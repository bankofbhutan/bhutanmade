<?php
/**
 * @Author      : Kien
 * @package     Marketplace_Seller_Membership
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\SellerMembership\Block\Adminhtml;

class SellerMembership extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_sellerMembership';
        $this->_blockGroup = 'Magetop_SellerMembership';
        $this->_headerText = __('Seller Membership');
        $this->_addButtonLabel = __('Add Seller Membership');
        parent::_construct();
    }
}