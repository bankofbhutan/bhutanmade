/**
 * @Author      : Kien
 * @package     Marketplace_Seller_Store_Locator
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
var config = {
	map: {
        '*': 
        {
			mk_seller_store_locator_map_js : 'Magetop_SellerStoreLocator/js/mk_maps',
            mk_seller_store_locator_map_seller_js : 'Magetop_SellerStoreLocator/js/mk_maps_seller'
		}
	}
};