<?php
/**
 * @Author      : Kien
 * @package     Marketplace_Seller_Store_Locator
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\SellerStoreLocator\Block\Adminhtml\SellerStoreLocator\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sellerstorelocator_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Store Locator Information'));
    }
}