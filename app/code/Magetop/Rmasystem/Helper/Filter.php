<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Helper;

use Magento\Framework\Session\SessionManager;

class Filter
{
    /**
     * @var Session
     */
    protected $session;

    public function __construct(
        SessionManager $session
    ) {
    
        $this->session = $session;
    }

    /**
     * @return array
     */
    public function getSortingSession()
    {
        return $this->session->getSortingSession();
    }
    /**
     * @return array
     */
    public function getFilterSession()
    {
        return $this->session->getFilterData();
    }
    /**
     * @return array
     */
    public function getGuestSortingSession()
    {
        return $this->session->getGuestSortingSession();
    }
    /**
     * @return array
     */
    public function getGuestFilterSession()
    {
        return $this->session->getGuestFilterData();
    }
    /**
     * @return array
     */
    public function getNewRmaSortingSession()
    {
        return $this->session->getNewRmaSortingSession();
    }
    /**
     * @return array
     */
    public function getNewRmaFilterSession()
    {
        return $this->session->getNewRmaFilterData();
    }
    /**
     * @return array
     */
    public function getNewGuestRmaSortingSession()
    {
        return $this->session->getNewGuestSortingSession();
    }
    /**
     * @return array
     */
    public function getNewGuestRmaFilterSession()
    {
        return $this->session->getNewGuestFilterData();
        ;
    }
}
