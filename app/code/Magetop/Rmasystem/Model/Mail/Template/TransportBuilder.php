<?php
namespace Magetop\Rmasystem\Model\Mail\Template;

class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{
    public function addAttachment(
        $filepath, $filename
    ) {
        if ($filename!=='') {
            $mimeType    = \Zend_Mime::TYPE_OCTETSTREAM;
            $disposition = \Zend_Mime::DISPOSITION_ATTACHMENT;
            $encoding    = \Zend_Mime::ENCODING_BASE64;

            $this->message->createAttachment(file_get_contents($filepath), $mimeType, $disposition, 
                $encoding, $filename);
        }
        return $this;
    }
}