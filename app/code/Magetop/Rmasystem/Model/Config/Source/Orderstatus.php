<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Model\Config\Source;

class Orderstatus implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label'=>__('Select Status Type'), 'value' => ''];
        $options[] = ['label'=>__('Complete'), 'value' => 'complete'];
        $options[] =['label'=>__('All Status'), 'value' => 'all'];
        return $options;
    }
}
