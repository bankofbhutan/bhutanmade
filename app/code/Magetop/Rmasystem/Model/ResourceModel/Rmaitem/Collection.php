<?php
namespace Magetop\Rmasystem\Model\ResourceModel\Rmaitem;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magetop\Rmasystem\Model\Rmaitem', 'Magetop\Rmasystem\Model\ResourceModel\Rmaitem');
    }
}
