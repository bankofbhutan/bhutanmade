<?php
namespace Magetop\Rmasystem\Model\ResourceModel\Shippinglabel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magetop\Rmasystem\Model\Shippinglabel', 'Magetop\Rmasystem\Model\ResourceModel\Shippinglabel');
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
