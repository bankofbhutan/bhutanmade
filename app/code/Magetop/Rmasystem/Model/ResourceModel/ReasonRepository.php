<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Model\ResourceModel;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magetop\Rmasystem\Model\ResourceModel\Reason\Collection;

/**
 * Rma reason CRUD class
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ReasonRepository implements \Magetop\Rmasystem\Api\ReasonRepositoryInterface
{
    /**
     * @var \Magetop\Rmasystem\Model\ReasonFactory
     */
    protected $reasonFactory;

    /**
     * @var \Magetop\Rmasystem\Api\Data\ReasonInterfaceFactory
     */
    protected $reasonDataFactory;

    /**
     * @var \Magetop\Rmasystem\Model\ResourceModel\Reason
     */
    protected $reasonResourceModel;

    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magetop\Rmasystem\Api\Data\ReasonSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @param \Magento\Customer\Model\GroupFactory $groupFactory
     * @param \Magento\Customer\Api\Data\GroupInterfaceFactory $groupDataFactory
     * @param \Magento\Customer\Model\ResourceModel\Group $groupResourceModel
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Customer\Api\Data\GroupSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Tax\Api\TaxClassRepositoryInterface $taxClassRepositoryInterface
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     */
    public function __construct(
        \Magetop\Rmasystem\Model\ReasonFactory $reasonFactory,
        \Magetop\Rmasystem\Api\Data\ReasonInterfaceFactory $reasonDataFactory,
        \Magetop\Rmasystem\Model\ResourceModel\Reason $reasonResourceModel,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magetop\Rmasystem\Api\Data\ReasonSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->reasonFactory = $reasonFactory;
        $this->reasonDataFactory = $reasonDataFactory;
        $this->reasonResourceModel = $reasonResourceModel;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magetop\Rmasystem\Api\Data\ReasonInterface $reason)
    {
        //$this->_validate($group);

        /** @var \Magetop\Rmasystem\Model\Reason $reasonModel */
        $reasonModel = null;
        if ($reason->getId() || (string)$reason->getId() === '0') {
            $groupDataAttributes = $this->dataObjectProcessor->buildOutputDataArray(
                $reason,
                '\Magetop\Rmasystem\Api\Data\ReasonInterface'
            );
            $reasonModel = $reason;
            $reasonModel->setData($groupDataAttributes);
        } else {
            $reasonModel = $this->reasonFactory->create();
            $reasonModel->setData($reason->getData());
        }
        try {
            $this->reasonResourceModel->save($reasonModel);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($e->getMessage() == (string)__('Could not save the record.')) {
                throw new InvalidTransitionException(__('Could not save the record.'));
            }
            throw $e;
        }

        $reasonDataObject = $this->reasonDataFactory->create()
            ->setData($reasonModel->getData());
        return $reasonDataObject;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($entityId)
    {
        $reason = $this->reasonFactory->create();
        $this->reasonResourceModel->load($reason, $entityId);
        if (!$reason->getId()) {
            throw new NoSuchEntityException(__('Record with id "%1" does not exist.', $entityId));
        }
        return $reason;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);

        /** @var \Magetop\Rmasystem\Model\ResourceModel\Reason\Collection $collection */
        $collection = $this->reasonFactory->create()->getCollection();
        $reasonInterfaceName = 'Magetop\Rmasystem\Api\Data\ReasonInterface';

        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType(): 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Magetop\Rmasystem\Api\Data\ReasonInterface[] $groups */
        $reasons = [];
        /** @var \Magetop\Rmasystem\Model\Reason $reason */
        foreach ($collection as $reason) {
            /** @var \Magento\Rmasystem\Api\Data\ReasonInterface $reasonDataObject */
            $reasonDataObject = $this->reasonDataFactory->create()
                ->setData($reason->getData());
            $reasons[] = $groupDataObject;
        }
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult->setItems($reasons);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Magetop\Rmasystem\Api\Data\ReasonInterface $reason)
    {
        try {
            $this->reasonResourceModel->delete($reason);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($reasonId)
    {
        return $this->delete($this->getById($reasonId));
    }
}
