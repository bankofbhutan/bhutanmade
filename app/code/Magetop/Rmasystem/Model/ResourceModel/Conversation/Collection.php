<?php
namespace Magetop\Rmasystem\Model\ResourceModel\Conversation;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magetop\Rmasystem\Model\Conversation', 'Magetop\Rmasystem\Model\ResourceModel\Conversation');
    }
}
