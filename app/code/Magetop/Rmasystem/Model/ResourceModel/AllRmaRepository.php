<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Model\ResourceModel;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magetop\Rmasystem\Model\ResourceModel\Allrma\Collection;

/**
 * Rma rma CRUD class
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class AllRmaRepository implements \Magetop\Rmasystem\Api\AllRmaRepositoryInterface
{
    /**
     * @var \Magetop\Rmasystem\Model\AllrmaFactory
     */
    protected $rmaFactory;

    /**
     * @var \Magetop\Rmasystem\Api\Data\AllrmaInterfaceFactory
     */
    protected $rmaDataFactory;

    /**
     * @var \Magetop\Rmasystem\Model\ResourceModel\Allrma
     */
    protected $rmaResourceModel;

    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magetop\Rmasystem\Api\Data\AllrmaSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @param \Magento\Customer\Model\GroupFactory $groupFactory
     * @param \Magento\Customer\Api\Data\GroupInterfaceFactory $groupDataFactory
     * @param \Magento\Customer\Model\ResourceModel\Group $groupResourceModel
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Customer\Api\Data\GroupSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Tax\Api\TaxClassRepositoryInterface $taxClassRepositoryInterface
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     */
    public function __construct(
        \Magetop\Rmasystem\Model\AllrmaFactory $rmaFactory,
        \Magetop\Rmasystem\Api\Data\AllrmaInterfaceFactory $rmaDataFactory,
        \Magetop\Rmasystem\Model\ResourceModel\Allrma $rmaResourceModel,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magetop\Rmasystem\Api\Data\AllrmaSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->rmaFactory = $rmaFactory;
        $this->rmaDataFactory = $rmaDataFactory;
        $this->rmaResourceModel = $rmaResourceModel;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magetop\Rmasystem\Api\Data\AllrmaInterface $rma)
    {
        //$this->_validate($group);

        /** @var \Magetop\Rmasystem\Model\Allrma $rmaModel */
        $rmaModel = null;
        if ($rma->getId() || (string)$rma->getId() === '0') {
            $rmaModel = $this->rmaFactory->create()->load($rma->getId());
            $rmaModel->setData($rma->getData());
        } else {
            $rmaModel = $this->rmaFactory->create();
            $rmaModel->setData($rma->getData());
        }
        try {
            $this->rmaResourceModel->save($rmaModel);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($e->getMessage() == (string)__('Could not save the record.')) {
                throw new InvalidTransitionException(__('Could not save the record.'));
            }
            throw $e;
        }

        $rmaDataObject = $this->rmaDataFactory->create()
            ->setData($rmaModel->getData());
        return $rmaDataObject;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($entityId)
    {
        $rma = $this->rmaFactory->create();
        $this->rmaResourceModel->load($rma, $entityId);
        if (!$rma->getId()) {
            throw new NoSuchEntityException(__('Record with id "%1" does not exist.', $entityId));
        }
        return $rma;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);

        /** @var \Magetop\Rmasystem\Model\ResourceModel\Allrma\Collection $collection */
        $collection = $this->rmaFactory->create()->getCollection();
        $rmaInterfaceName = 'Magetop\Rmasystem\Api\Data\AllrmaInterface';

        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType(): 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Magetop\Rmasystem\Api\Data\AllrmaInterface[] $groups */
        $rmas = [];
        /** @var \Magetop\Rmasystem\Model\Allrma $rma */
        foreach ($collection as $rma) {
            /** @var \Magento\Rmasystem\Api\Data\AllrmaInterface $rmaDataObject */
            $rmaDataObject = $this->rmaDataFactory->create()
                ->setData($rma->getData());
            $rmas[] = $groupDataObject;
        }
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult->setItems($rmas);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Magetop\Rmasystem\Api\Data\AllrmaInterface $rma)
    {
        try {
            $this->rmaResourceModel->delete($rma);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($rmaId)
    {
        return $this->delete($this->getById($rmaId));
    }
}
