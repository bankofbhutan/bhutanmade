<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Model\ResourceModel;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magetop\Rmasystem\Model\ResourceModel\Conversation\Collection;

/**
 * Rma conversation CRUD class
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ConversationRepository implements \Magetop\Rmasystem\Api\ConversationRepositoryInterface
{
    /**
     * @var \Magetop\Rmasystem\Model\ConversationFactory
     */
    protected $conversationFactory;

    /**
     * @var \Magetop\Rmasystem\Api\Data\ConversationInterfaceFactory
     */
    protected $conversationDataFactory;

    /**
     * @var \Magetop\Rmasystem\Model\ResourceModel\Conversation
     */
    protected $conversationResourceModel;

    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magetop\Rmasystem\Api\Data\ConversationSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @param \Magento\Customer\Model\GroupFactory $groupFactory
     * @param \Magento\Customer\Api\Data\GroupInterfaceFactory $groupDataFactory
     * @param \Magento\Customer\Model\ResourceModel\Group $groupResourceModel
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Customer\Api\Data\GroupSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Tax\Api\TaxClassRepositoryInterface $taxClassRepositoryInterface
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     */
    public function __construct(
        \Magetop\Rmasystem\Model\ConversationFactory $conversationFactory,
        \Magetop\Rmasystem\Api\Data\ConversationInterfaceFactory $conversationDataFactory,
        \Magetop\Rmasystem\Model\ResourceModel\Conversation $conversationResourceModel,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magetop\Rmasystem\Api\Data\ConversationSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->conversationFactory = $conversationFactory;
        $this->conversationDataFactory = $conversationDataFactory;
        $this->conversationResourceModel = $conversationResourceModel;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magetop\Rmasystem\Api\Data\ConversationInterface $conversation)
    {
        //$this->_validate($group);

        /** @var \Magetop\Rmasystem\Model\Conversation $conversationModel */
        $conversationModel = null;
        if ($conversation->getId() || (string)$conversation->getId() === '0') {
            $conversationModel = $this->conversationFactory->create()->load($conversation->getId());
            $groupDataAttributes = $this->dataObjectProcessor->buildOutputDataArray(
                $conversation,
                '\Magetop\Rmasystem\Api\Data\ConversationInterface'
            );
        } else {
            $conversationModel = $this->conversationFactory->create();
            $conversationModel->setData($conversation->getData());
        }
        try {
            $this->conversationResourceModel->save($conversationModel);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($e->getMessage() == (string)__('Could not save the record.')) {
                throw new InvalidTransitionException(__('Could not save the record.'));
            }
            throw $e;
        }

        $conversationDataObject = $this->conversationDataFactory->create()
            ->setData($conversationModel->getData());
        return $conversationDataObject;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($entityId)
    {
        $conversation = $this->conversationFactory->create();
        $this->conversationResourceModel->load($conversation, $entityId);
        if (!$conversation->getId()) {
            throw new NoSuchEntityException(__('Record with id "%1" does not exist.', $entityId));
        }
        return $conversation;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);

        /** @var \Magetop\Rmasystem\Model\ResourceModel\Conversation\Collection $collection */
        $collection = $this->conversationFactory->create()->getCollection();
        $conversationInterfaceName = 'Magetop\Rmasystem\Api\Data\ConversationInterface';

        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType(): 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Magetop\Rmasystem\Api\Data\ConversationInterface[] $groups */
        $conversations = [];
        /** @var \Magetop\Rmasystem\Model\Conversation $conversation */
        foreach ($collection as $conversation) {
            /** @var \Magento\Rmasystem\Api\Data\ConversationInterface $conversationDataObject */
            $conversationDataObject = $this->conversationDataFactory->create()
                ->setData($conversation->getData());
            $conversations[] = $groupDataObject;
        }
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult->setItems($conversations);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Magetop\Rmasystem\Api\Data\ConversationInterface $conversation)
    {
        try {
            $this->conversationResourceModel->delete($conversation);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($conversationId)
    {
        return $this->delete($this->getById($conversationId));
    }
}
