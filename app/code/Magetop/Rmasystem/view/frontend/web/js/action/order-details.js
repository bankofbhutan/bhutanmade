/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'mage/url',
    ],
    function ($, urlBuilder) {
        'use strict';

        return function (orderData) {
            var serviceUrl,
                payload;

            var contentType = 'application/json';

            /**
             * Checkout for guest and registered customer.
             */
            serviceUrl = 'rest/V1/customer/order-details';
            payload = {
                orderId: orderData.entity_id
            };
            return $.ajax({
                url: urlBuilder.build(serviceUrl),
                type: 'POST',
                showLoader: true,
                data: JSON.stringify(payload),
                global: true,
                contentType: contentType
            });
        };
    }
);
