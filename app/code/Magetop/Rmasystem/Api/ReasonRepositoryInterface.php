<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Api;

/**
 * Rma reason CRUD interface
 * @api
 */
interface ReasonRepositoryInterface
{
    /**
     * Save Reason.
     *
     * @param Magetop\Rmasystem\Api\Data\ReasonInterface $reason
     * @return Magetop\Rmasystem\Api\Data\ReasonInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If a RMA reason ID is sent but it does not exist
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magetop\Rmasystem\Api\Data\ReasonInterface $reason);

    /**
     * Get reason by rma ID.
     *
     * @param int $id
     * @return Magetop\Rmasystem\Api\Data\ReasonInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $rmaId is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve rma reason list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Magetop\Rmasystem\Api\Data\ReasonSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete rma reason.
     *
     * @param Magetop\Rmasystem\Api\Data\ReasonInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\StateException If rma reason cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magetop\Rmasystem\Api\Data\ReasonInterface $group);

    /**
     * Delete rma reason by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException If rma reason cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
