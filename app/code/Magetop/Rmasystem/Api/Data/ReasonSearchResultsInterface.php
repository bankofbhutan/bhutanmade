<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */

namespace Magetop\Rmasystem\Api\Data;

/**
 * Interface for rma reason search results.
 * @api
 */
interface ReasonSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get reason list.
     *
     * @return \Magetop\Rmasystem\Api\Data\ReasonInterface[]
     */
    public function getItems();

    /**
     * Set reason list.
     *
     * @api
     * @param \Magetop\Rmasystem\Api\Data\ReasonInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
