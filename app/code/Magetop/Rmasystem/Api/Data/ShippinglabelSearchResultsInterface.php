<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */

namespace Magetop\Rmasystem\Api\Data;

/**
 * Interface for rma shipping label search results.
 * @api
 */
interface ShippinglabelSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get shipping label list.
     *
     * @return \Magetop\Rmasystem\Api\Data\ShippingLabelInterface[]
     */
    public function getItems();

    /**
     * Set shipping label list.
     *
     * @api
     * @param \Magetop\Rmasystem\Api\Data\ShippingLabelInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
