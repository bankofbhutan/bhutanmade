<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */

namespace Magetop\Rmasystem\Api\Data;

/**
 * Interface for returned items search results.
 * @api
 */
interface RmaitemSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get returned items list.
     *
     * @return \Magetop\Rmasystem\Api\Data\RmaitemInterface[]
     */
    public function getItems();

    /**
     * Set returned items list.
     *
     * @api
     * @param \Magetop\Rmasystem\Api\Data\RmaitemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
