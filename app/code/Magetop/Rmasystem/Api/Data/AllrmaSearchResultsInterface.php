<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */

namespace Magetop\Rmasystem\Api\Data;

/**
 * Interface for all rma search results.
 * @api
 */
interface AllrmaSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get all rma list.
     *
     * @return \Magetop\Rmasystem\Api\Data\AllrmaInterface[]
     */
    public function getItems();

    /**
     * Set all rma list.
     *
     * @api
     * @param \Magetop\Rmasystem\Api\Data\AllrmaInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
