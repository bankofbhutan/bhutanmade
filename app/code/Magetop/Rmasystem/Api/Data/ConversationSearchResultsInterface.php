<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */

namespace Magetop\Rmasystem\Api\Data;

/**
 * Interface for rma conversation search results.
 * @api
 */
interface ConversationSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get conversation list.
     *
     * @return \Magetop\Rmasystem\Api\Data\ConversationInterface[]
     */
    public function getItems();

    /**
     * Set conversation list.
     *
     * @api
     * @param \Magetop\Rmasystem\Api\Data\ConversationInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
