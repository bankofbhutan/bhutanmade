<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
 namespace Magetop\Rmasystem\Api\Data;

interface ConversationInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID                = 'id';
    const RMA_ID            = 'rma_id';
    const MESSAGE           = 'message';
    const CREATED_AT        = 'created_at';
    const SENDER            = 'sender';

    /**
     * Get Id
     * @return int
     */
    public function getId();

    /**
     * get rma id
     * @return int
     */
    public function getRmaId();

    /**
     * get Message
     * @return string
     */
    public function getMessage();

    /**
     * get creation time.
     * @return string
     */
    public function getCreatedAt();

    /**
     * get Sender
     * @return string
     */
    public function getSender();

    /**
     * set Id
     * @return \Magetop\Rmasystem\Api\Data\ConversationInterface
     */
    public function setId($id);

    /**
     * set rma id
     * @return \Magetop\Rmasystem\Api\Data\ConversationInterface
     */
    public function setRmaId($rmaId);

    /**
     * set Message
     * @return \Magetop\Rmasystem\Api\Data\ConversationInterface
     */
    public function setMessage($message);

    /**
     * set creation time.
     * @return \Magetop\Rmasystem\Api\Data\ConversationInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * set Sender
     * @return \Magetop\Rmasystem\Api\Data\ConversationInterface
     */
    public function setSender($sender);
}
