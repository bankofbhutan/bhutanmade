<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Api;

/**
 * Rma item CRUD interface
 * @api
 */
interface RmaitemRepositoryInterface
{
    /**
     * Save RMA item.
     *
     * @param Magetop\Rmasystem\Api\Data\RmaitemInterface $rmaItem
     * @return Magetop\Rmasystem\Api\Data\RmaitemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If a RMA ID is sent but the rma does not exist
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magetop\Rmasystem\Api\Data\RmaitemInterface $rmaItem);

    /**
     * Get RMA item by ID.
     *
     * @param int $id
     * @return Magetop\Rmasystem\Api\Data\RmaitemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $rmaId is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve rma item list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Magetop\Rmasystem\Api\Data\RmaitemSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete rma item.
     *
     * @param Magetop\Rmasystem\Api\Data\RmaitemInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magetop\Rmasystem\Api\Data\RmaitemInterface $rmaItem);

    /**
     * Delete RMA item by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
