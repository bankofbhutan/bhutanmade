<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Api;

interface ApplyFilterInterface
{
    /**
     * Returns selected order detail
     *
     * @api
     * @param string $orderId
     * @param string $price
     * @param string $date
     * @return string.
     */
    public function applyFilter($orderId = 0, $price = null, $date = null);
}
