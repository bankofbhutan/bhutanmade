<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Api;

interface OrderDetailsInterface
{
    /**
     * Returns selected order detail
     *
     * @api
     * @param int $orderId
     * @return string.
     */
    public function getDetails($orderId);
}
