<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Api;

/**
 * All rma CRUD interface
 * @api
 */
interface AllRmaRepositoryInterface
{
    /**
     * Save RMA.
     *
     * @param Magetop\Rmasystem\Api\Data\AllrmaInterface $allrma
     * @return Magetop\Rmasystem\Api\Data\AllrmaInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If a RMA ID is sent but the rma does not exist
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magetop\Rmasystem\Api\Data\AllrmaInterface $allrma);

    /**
     * Get RMA by rma ID.
     *
     * @param int $id
     * @return Magetop\Rmasystem\Api\Data\AllrmaInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $rmaId is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve rma list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Magetop\Rmasystem\Api\Data\AllrmaSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete rma.
     *
     * @param Magetop\Rmasystem\Api\Data\AllrmaInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magetop\Rmasystem\Api\Data\AllrmaInterface $group);

    /**
     * Delete RMA by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
