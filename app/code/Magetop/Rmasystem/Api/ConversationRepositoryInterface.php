<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Api;

/**
 * Rma Conversation CRUD interface
 * @api
 */
interface ConversationRepositoryInterface
{
    /**
     * Save Conversation.
     *
     * @param Magetop\Rmasystem\Api\Data\ConversationInterface $conversation
     * @return Magetop\Rmasystem\Api\Data\ConversationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magetop\Rmasystem\Api\Data\ConversationInterface $conversation);

    /**
     * Get Conversation by ID.
     *
     * @param int $id
     * @return Magetop\Rmasystem\Api\Data\ConversationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $rmaId is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve conversation list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Magetop\Rmasystem\Api\Data\ConversationSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete conversation.
     *
     * @param Magetop\Rmasystem\Api\Data\ConversationInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magetop\Rmasystem\Api\Data\ConversationInterface $group);

    /**
     * Delete conversation by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
