<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Update tables 'marketplace_saleslist'
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('magetop_rma_items'),
            'order_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
                'comment' => 'Order Id'
            ]
        );

        /**
         * Update tables 'marketplace_saleslist'
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('magetop_rma'),
            'name',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'default' => null,
                'comment' => 'Customer Name'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('magetop_rma'),
            'admin_status',
            [
              'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              'unsigned' => true,
              'nullable' => false,
              'default' => '0',
              'comment' => 'Admin Final Status'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('magetop_rma'),
            'final_status',
            [
              'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              'unsigned' => true,
              'nullable' => false,
              'default' => '0',
              'comment' => 'Final Status'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('magetop_rma_conversation'),
            'attachment',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'default' => null,
                'comment' => 'Attachment'
            ]
        );

        /**
         * Add foreign keys
         */
        $setup->getConnection()->addForeignKey(
            $setup->getFkName(
                'magetop_rma_items',
                'rma_id',
                'magetop_rma',
                'rma_id'
            ),
            $setup->getTable('magetop_rma_items'),
            'rma_id',
            $setup->getTable('magetop_rma'),
            'rma_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $setup->getConnection()->addForeignKey(
            $setup->getFkName(
                'rma_conversation',
                'rma_id',
                'magetop_rma',
                'rma_id'
            ),
            $setup->getTable('magetop_rma_conversation'),
            'rma_id',
            $setup->getTable('magetop_rma'),
            'rma_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        
        /**
         * create table magetop_rma_customfield
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magetop_rma_customfield'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'input_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'input type'
            )
            ->addColumn(
                'label',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'label'
            )
            ->addColumn(
                'inputname',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'inputname'
            )
            ->addColumn(
                'select_option',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false, 'default' => null],
                'select_option'
            )
            ->addColumn(
                'validationtype',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'validationtype'
            )
            ->addColumn(
                'required',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => 0],
                'required'
            )
            ->addColumn(
                'status',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => 0],
                'status'
            )
            ->addColumn(
                'sort',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => 0],
                'sort'
            )
            ->setComment('Rma Custom field Table');
        $installer->getConnection()->createTable($table);

        /**
         * create table magetop_rma_customfield_value
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magetop_rma_customfield_value'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'field_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'field id'
            )
            ->addColumn(
                'rma_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'rma id'
            )
            ->addColumn(
                'value',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true, 'default' => null],
                'value'
            )
        ->setComment('Rma Custom field Values Table');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
