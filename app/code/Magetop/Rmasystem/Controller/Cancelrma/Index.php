<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Controller\Cancelrma;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlFactory;
use Magento\Framework\Filesystem\Io\File;

class Index extends \Magetop\Rmasystem\Controller\FrontController
{
    /**
     * Cancel action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        $customerId = $this->_objectManager->create('Magento\Customer\Model\Session')->getCustomerId();
        $model = $this->rmaRepository->getById($id);
        if ($model->getCustomerId() == $customerId) {
            $model->setStatus(4);
            $model->setFinalStatus(1);
            $model->setAdminStatus(0);
            $this->rmaRepository->save($model);

            $message = '<span>'.__("RMA Request Cancelled").'</span><br/><br/><p class="msg-content">'.
              __('Your RMA request has been cancelled successfully.').'</p>';
            $this->saveRmaHistory($id, $message);
            $this->_emailHelper->cancelRmaEmail($model);
            $this->messageManager->addSuccess(
                __('RMA with id ').$id.__(' has been cancelled successfully')
            );
            return $resultRedirect->setPath('*/index');
        } else {
            $this->messageManager->addError(
                __('Sorry You Are Not Authorised to cancel this RMA request')
            );
            return $resultRedirect->setPath('*/index');
        }
    }
}
