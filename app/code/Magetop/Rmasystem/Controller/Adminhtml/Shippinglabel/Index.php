<?php
namespace Magetop\Rmasystem\Controller\Adminhtml\Shippinglabel;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Magetop_Rmasystem::shippinglabel';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magetop_Rmasystem::shippinglabel');
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magetop_Rmasystem::shippinglabel');
        $resultPage->addBreadcrumb(__('RMA Shipping Label'), __('RMA Shipping Label'));
        $resultPage->getConfig()->getTitle()->prepend(__('RMA Shipping Label'));

        return $resultPage;
    }
}
