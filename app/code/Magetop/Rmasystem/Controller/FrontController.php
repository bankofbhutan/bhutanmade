<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Controller;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magetop\Rmasystem\Model\ResourceModel\Rmaitem\CollectionFactory as ItemCollectionFactory;
use Magento\Sales\Model\OrderRepository;
use Magetop\Rmasystem\Api\AllRmaRepositoryInterface;
use Magetop\Rmasystem\Api\Data\AllrmaInterfaceFactory;
use Magento\Framework\Session\SessionManager;
use Magento\ImportExport\Model\ResourceModel\Helper as ResourceModelHelper;

class FrontController extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magetop\Rmasystem\Helper\Email
     */
    protected $_emailHelper;

    /**
     * @var \Magetop\Rmasystem\Helper\Data
     */
    protected $helper;

    /**
     * @var File
     */
    protected $_fileIo;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var ItemCollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var \Magetop\Rmasystem\Api\Data\RmaitemInterfaceFactory
     */
    protected $rmaItemDataFactory;

    /**
     * @var \Magetop\Rmasystem\Api\RmaitemRepositoryInterface
     */
    protected $rmaItemRepository;

    /**
     * @var \Magetop\Rmasystem\Api\Data\ConversationInterfaceFactory
     */
    protected $conversationDataFactory;

    /**
     * @var \Magetop\Rmasystem\Api\ConversationRepositoryInterface
     */
    protected $conversationRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var AllRmaRepositoryInterface
     */
    protected $rmaRepository;

    /**
     * @var AllrmaInterfaceFactory
     */
    protected $rmaFactory;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var ResourceModelHelper
     */
    protected $resourceModelHelper;
    /**
     * @var Magetop\Rmasystem\Model\FieldvalueFactory
     */
    protected $fieldValue;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param \Magetop\Rmasystem\Helper\Email $emailHelper
     * @param \Magetop\Rmasystem\Helper\Data $helper
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param \Magetop\Rmasystem\Api\Data\RmaitemInterfaceFactory $rmaItemDataFactory
     * @param \Magetop\Rmasystem\Api\RmaitemRepositoryInterface $rmaItemRepository
     * @param \Magetop\Rmasystem\Api\Data\ConversationInterfaceFactory $conversationDataFactory
     * @param \Magetop\Rmasystem\Api\ConversationRepositoryInterface $conversationRepository
     * @param \Magetop\Rmasystem\Model\FieldvalueFactory $fieldValueFactory
     * @param OrderRepository $orderRepository
     * @param AllRmaRepositoryInterface $rmaRepository
     * @param AllrmaInterfaceFactory $rmaFactory
     * @param ResourceModelHelper $resourceModelHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param SessionManager $session
     * @param File $fileIo
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        \Magetop\Rmasystem\Helper\Email $emailHelper,
        \Magetop\Rmasystem\Helper\Data $helper,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ItemCollectionFactory $itemCollectionFactory,
        \Magetop\Rmasystem\Api\Data\RmaitemInterfaceFactory $rmaItemDataFactory,
        \Magetop\Rmasystem\Api\RmaitemRepositoryInterface $rmaItemRepository,
        \Magetop\Rmasystem\Api\Data\ConversationInterfaceFactory $conversationDataFactory,
        \Magetop\Rmasystem\Api\ConversationRepositoryInterface $conversationRepository,
        \Magetop\Rmasystem\Model\FieldvalueFactory $fieldValueFactory,
        OrderRepository $orderRepository,
        AllRmaRepositoryInterface $rmaRepository,
        AllrmaInterfaceFactory $rmaFactory,
        ResourceModelHelper $resourceModelHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        SessionManager $session,
        File $fileIo
    ) {

        $this->_customerSession = $customerSession;
        $this->_fileIo = $fileIo;
        $this->_emailHelper = $emailHelper;
        $this->helper = $helper;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->rmaItemDataFactory = $rmaItemDataFactory;
        $this->rmaItemRepository = $rmaItemRepository;
        $this->conversationDataFactory = $conversationDataFactory;
        $this->conversationRepository = $conversationRepository;
        $this->orderRepository = $orderRepository;
        $this->rmaRepository = $rmaRepository;
        $this->rmaFactory = $rmaFactory;
        $this->resourceModelHelper = $resourceModelHelper;
        $this->session = $session;
        $this->date = $date;
        $this->fieldValue = $fieldValueFactory;
        parent::__construct($context);
    }

    public function execute()
    {
      /** child controllers will use it */
    }

    protected function saveRmaProductImage($numberOfImages, $lastRmaId)
    {
          $imageArray = [];
        if ($numberOfImages > 0) {
            $path = $this->helper->getBaseDir($lastRmaId);
            for ($i = 0; $i < $numberOfImages; $i++) {
                $fileId = "related_images[$i]";
                $this->uploadImage($fileId, $path, $imageArray);
            }
        }
          return $imageArray;
    }

    /**
     * Notify message that rma created.
     * @param  int $rmaId
     */
    public function saveRmaHistory($rmaId, $message)
    {
        $conversationModel = $this->conversationDataFactory->create()
          ->setRmaId($rmaId)
          ->setMessage($message)
          ->setCreatedAt(time())
          ->setSender('default');
        try {
            $this->conversationRepository->save($conversationModel);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect->setPath('*/*/index');
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the Message.'));
            return $resultRedirect->setPath('*/*/index');
        }
    }

    /**
     * Upload Image of Rma
     *
     * @param string $fileId
     * @param string $uploadPath
     * @param int $count
     */
    protected function uploadImage($fileId, $path, &$imageArray)
    {
        $extArray = ['jpg','jpeg', 'gif','png'];
        try {
            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
            $uploader = $this->_fileUploaderFactory->create(['fileId' => $fileId]);
            $uploader->setAllowedExtensions($extArray);
            $uploader->setAllowRenameFiles(true);
            $uploader->setAllowCreateFolders(true);
            $result = $uploader->save($path);
            $imageArray[$result['file']] = $result['file'];
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving images.'));
        }
    }
}
