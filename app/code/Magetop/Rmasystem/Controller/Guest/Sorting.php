<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Rmasystem
 * @author    Magetop
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://www.magetop.com)
 * @license   https://www.magetop.com/license.html
 */
namespace Magetop\Rmasystem\Controller\Guest;

class Sorting extends \Magetop\Rmasystem\Controller\Index\Sorting
{
    /**
     * set sorting data
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        
        $this->_objectManager->create(
            'Magento\Framework\Session\SessionManager'
        )->unsetGuestSortingSession();
        $data = $this->getRequest()->getPost();

        $this->_objectManager->create(
            'Magento\Framework\Session\SessionManager'
        )->setGuestSortingSession($data);

        return $resultPage;
    }
}
