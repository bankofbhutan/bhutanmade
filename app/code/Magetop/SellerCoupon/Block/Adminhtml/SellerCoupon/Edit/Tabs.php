<?php
/**
 * @Author      Kien
 * @package     Marketplace_Seller_Coupon
 * @copyright   Copyright (c) 2020 MAGETOP (https://www.magetop.com)
 * @terms       https://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\SellerCoupon\Block\Adminhtml\SellerCoupon\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sellercoupon_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Coupon Information'));
    }
}