<?php
/**
 * @Author      Kien
 * @package     Marketplace_Seller_Coupon
 * @copyright   Copyright (c) 2020 MAGETOP (https://www.magetop.com)
 * @terms       https://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Magetop_SellerCoupon',
    __DIR__
);