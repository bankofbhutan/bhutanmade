/**
 * Copyright � 2015 Scommerce Mage. All rights reserved.
 * See COPYING.txt for license details.
 */
 
var config = {
    map: {
        '*': {
            "Magento_Checkout/js/view/shipping" : "Magetop_SellerFlatRateShipping/js/view/shipping",
            "Magento_Checkout/js/model/shipping-save-processor/default" : "Magetop_SellerFlatRateShipping/js/shipping-save-processor-default-override"
        }
    }
};