<?php
/**
 * @Author      : haunv
 * @package     Marketplace
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
 namespace Magetop\Marketplace\Block\Product\Component;
 
 class StepsWizard extends \Magento\Ui\Block\Component\StepsWizard {
	 
    /**
     * Wizard step template
     *
     * @var string
     */
    //protected $_template = 'Magetop_Marketplace::product/configurable/stepswizard.phtml';	
 }