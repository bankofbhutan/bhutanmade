<?php
/**
 * @Author      : haunv
 * @package     Marketplace
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\Marketplace\Block\Product\CustomOpt\Options\Type;

class File extends \Magetop\Marketplace\Block\Product\CustomOpt\Options\Type\AbstractType
{
    /**
     * @var string
     */
    protected $_template = 'Magetop_Marketplace::product/customoptions/options/type/file.phtml';
}
