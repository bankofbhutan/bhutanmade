<?php
/**
 * @Author      : Kien
 * @package     Marketplace
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\Marketplace\Block\Adminhtml\Payments\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('payments_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Payment Information'));
    }
	
	protected function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }
}
