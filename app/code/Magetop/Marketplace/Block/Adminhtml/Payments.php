<?php
/**
 * @Author      : Kien
 * @package     Marketplace
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
 
namespace Magetop\Marketplace\Block\Adminhtml;
use Magento\Backend\Block\Widget\Grid\Container;

class Payments extends Container
{
   /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_payments';
        $this->_blockGroup = 'Magetop_Marketplace';
        $this->_headerText = __('Manage Payment Method');
        $this->_addButtonLabel = __('Add New Payment Method');
        parent::_construct();
    }
}