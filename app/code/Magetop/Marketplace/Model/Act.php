<?php 
/**
* @package    Magetop_Marketplace
* @version    2.0
* @author     Magetop Developer Team <magetop99@gmail.com>
* @website    https://www.magetop.com/magento-multi-vendor-marketplace-extension
* @copyright  Copyright (c) 2009-2020 MAGETOP.COM. (http://www.magetop.com)
*/
namespace Magetop\Marketplace\Model;
class Act extends \Magento\Framework\Model\AbstractModel {
    /**
     * Initialize resource model
     * @return void
     */
    public function _construct() {
        $this->_init('Magetop\Marketplace\Model\ResourceModel\Act');
    }
}