<?php
/**
* @package    Magetop_Marketplace
* @version    2.0
* @author     Magetop Developer Team <magetop99@gmail.com>
* @website    https://www.magetop.com/magento-multi-vendor-marketplace-extension
* @copyright  Copyright (c) 2009-2020 MAGETOP.COM. (http://www.magetop.com)
*/
namespace Magetop\Marketplace\Model\ResourceModel\Act;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Constructor
     * Configures collection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magetop\Marketplace\Model\Act', 'Magetop\Marketplace\Model\ResourceModel\Act');
    }
}