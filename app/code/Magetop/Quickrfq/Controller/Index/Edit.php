<?php
namespace Magetop\Quickrfq\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magetop\Quickrfq\Model\SellerquickrfqFactory;

class Edit extends \Magento\Framework\App\Action\Action
{
	protected $_sellerquickrfqFactory;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
    */
    public function __construct(
        Context $context,
		SellerquickrfqFactory $sellerquickrfqFactory,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
		$this->_sellerquickrfqFactory = $sellerquickrfqFactory;
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute()
    {
		$customer = $this->_objectManager->create('Magento\Customer\Model\Session');	
		if ( $customer->getId() < 1 )	{ 
            $this->_redirect('customer/account/login');
		}
		
		$Id = $this->getRequest()->getParam('id');
		$model = $this->_sellerquickrfqFactory->create();
		$messages = $model->load($Id );
		
		$this->_objectManager->get('Magento\Framework\Registry')->register('SellerquickrfqData', $messages);
 
        $pageFactory = $this->resultPageFactory->create();
		$pageFactory->getConfig()->getTitle()->set( 'Edit RFQ' );
		$this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}