<?php
namespace Magetop\Quickrfq\Block;
class View extends \Magento\Framework\View\Element\Template
{
    protected $_gridFactory; 
    protected $_replyFactory;   
            
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\App\ResourceConnection $resource,
        \Magetop\Quickrfq\Model\SellerquickrfqFactory $gridFactory,          
		\Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        $this->_gridFactory = $gridFactory;              
		$this->_customerSession = $customerSession;
		$this->_resource = $resource;
        parent::__construct($context, $data);
    }
    
    /**
	* get list Quickrfq
	* @return $items
	**/
	function getQuickrfq()
	{
	   	$customerSession = $this->_customerSession;
		$sellerid = $customerSession->getId();
	   	$collection = null;
        if($sellerid > 0)
		{
            //get collection of data 
            $collection = $this->_gridFactory->create()->getCollection();
    		$collection->getSelect()->where('main_table.seller_id=?', $this->_customerSession->getId());
            $collection->setOrder('quickrfq_id', 'DESC' );
            $limit = $this->getRequest()->getParam('limit',12);
			if($limit > 0)
			{
				$collection->setPageSize($limit);
			}
            $curPage = $this->getRequest()->getParam('p',1);
			if($curPage > 1)
			{
				$collection->setCurPage($curPage);
			}
        }
        return $collection;
	}
    protected function _prepareLayout()
    {
        $collection = $this->getMessages();
        parent::_prepareLayout();
        if ($collection) {
            // create pager block for collection
            $pager = $this->getLayout()->createBlock('Magento\Theme\Block\Html\Pager','my.custom.pager');
            $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all')); 
            $pager->setCollection($collection);
            $this->setChild('pager', $pager);
            $collection->load();
        }
        return $this;
    }
    /**
     * @return string
     */
    // method for get pager html
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }  
}
?>