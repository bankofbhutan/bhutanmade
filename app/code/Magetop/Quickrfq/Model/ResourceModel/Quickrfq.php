<?php

namespace Magetop\Quickrfq\Model\ResourceModel;

class Quickrfq extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{    
    protected function _construct()
    {
        $this->_init('magetop_quickrfq', 'quickrfq_id');
    }
}
