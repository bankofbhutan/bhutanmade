<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magetop\Quickrfq\Model\ResourceModel\Sellerquickrfq;

use Magetop\Quickrfq\Model\ResourceModel\AbstractCollection;

/**
 * CMS page collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'quickrfq_id';

    
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magetop\Quickrfq\Model\Sellerquickrfq', 'Magetop\Quickrfq\Model\ResourceModel\Sellerquickrfq');
        $this->_map['fields']['quickrfq_id'] = 'main_table.quickrfq_id';
    }

    
    public function addStoreFilter($store, $withAdmin = true)
    {
        return $this;
    }
}
