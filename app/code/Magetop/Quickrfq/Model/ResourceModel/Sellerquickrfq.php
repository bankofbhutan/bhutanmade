<?php

namespace Magetop\Quickrfq\Model\ResourceModel;

class Sellerquickrfq extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{     
    protected function _construct()
    {
        $this->_init('magetop_seller_quickrfq', 'quickrfq_id');
    }
}
