<?php
/**
 * @Author      Magetop Developers
 * @package     Magetop_GiftCard
 * @copyright   Copyright (c) 2019 MAGETOP (https://www.magetop.com)
 * @terms       https://www.magetop.com/terms
 * @license     https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\GiftCard\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Backend system config datetime field renderer
 */
class Info extends \Magento\Config\Block\System\Config\Form\Field
{

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     * @return string
     * @codeCoverageIgnore
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $html = '
                <div style="text-align: center">
                <h1 style="text-align:center">Marketplace Extensions</h3>
                Developed by Cloud Bhutan for Bhutan Made. For help and support, visit <a target="_blank" href="https://www.cloudbhutan.com/" >www.cloudbhutan.com</a>
                </div>
                ';
        
        return $html;
    }
}