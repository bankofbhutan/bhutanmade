<?php
/**
 * @Author      Magetop Developers
 * @package     Magetop_GiftCard
 * @copyright   Copyright (c) 2019 MAGETOP (https://www.magetop.com)
 * @terms       https://www.magetop.com/terms
 * @license     https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\GiftCard\Model\Product\Type;

class GiftCard extends \Magento\Catalog\Model\Product\Type\Virtual
{
    const TYPE_ID = "giftcard";
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
        
    }
}
