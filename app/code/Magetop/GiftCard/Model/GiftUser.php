<?php
/**
 * @Author      Magetop Developers
 * @package     Magetop_GiftCard
 * @copyright   Copyright (c) 2019 MAGETOP (https://www.magetop.com)
 * @terms       https://www.magetop.com/terms
 * @license     https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\GiftCard\Model;
 
use Magento\Framework\Model\AbstractModel;
 
class GiftUser extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magetop\GiftCard\Model\ResourceModel\GiftUser');
    }
}
