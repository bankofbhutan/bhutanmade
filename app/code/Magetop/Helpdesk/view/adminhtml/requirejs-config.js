/**
 * @category   Magetop
 * @package    Magetop_Helpdesk
 * @author     Magetop Software Private Limited
 * @copyright  Copyright (c) Magetop Software Private Limited (https://magetop.com)
 * @license    https://store.magetop.com/license.html
 */
var config = {
    map: {
        '*': {
            ticketList: "Magetop_Helpdesk/js/ticketsGrid.js",
            ticketThread: "Magetop_Helpdesk/js/ticketThread.js"
        }
    }
};