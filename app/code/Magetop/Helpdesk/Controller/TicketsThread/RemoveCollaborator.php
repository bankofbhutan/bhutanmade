<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Helpdesk
 * @author    Magetop Software Private Limited
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://magetop.com)
 * @license   https://store.magetop.com/license.html
 */

namespace Magetop\Helpdesk\Controller\TicketsThread;

use Magetop\Helpdesk\Controller\AbstractController;

class RemoveCollaborator extends AbstractController
{
    /** @var \Magento\Framework\View\Result\PageFactory */
    private $resultPageFactory;

    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    private $jsonResultFactory;

    /** @var \Helpdesk\Model\TicketManagerCustomer */
    private $ticketManagerCustomer;

    /** @var \Magetop\Helpdesk\Helper\Tickets */
    private $ticketsHelper;

    /**
     * __construct function
     *
     * @param \Magento\Backend\App\Action\Context                 $context
     * @param \Magento\Framework\Controller\Result\JsonFactory    $jsonResultFactory
     * @param \Magetop\Helpdesk\Model\TicketManagerCustomer $ticketManagerCustomer
     * @param \Magetop\Helpdesk\Helper\Tickets              $ticketsHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \Magetop\Helpdesk\Model\TicketManagerCustomer $ticketManagerCustomer,
        \Magetop\Helpdesk\Helper\Tickets $ticketsHelper
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->ticketManagerCustomer = $ticketManagerCustomer;
        $this->ticketsHelper = $ticketsHelper;
        parent::__construct($context, $resultPageFactory);
    }

    public function execute()
    {
        $result = $this->jsonResultFactory->create();
        $successCount = 0;
        $errorCount = 0;
        $post = $this->getRequest()->getParams();
        if ((isset($post['ticketId']) &&
            !empty($post['ticketId'])) &&
            (isset($post['collaboratorId']) &&
            !empty($post['collaboratorId']))
        ) {
            $response = $this->ticketManagerCustomer->removeCollaborater($post['ticketId'], $post['collaboratorId']);
            return $result->setData($response);
        }
    }

    /*
     * Check permission via ACL resource
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magetop_Helpdesk::tickets');
    }
}
