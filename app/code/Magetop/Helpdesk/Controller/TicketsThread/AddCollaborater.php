<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Helpdesk
 * @author    Magetop Software Private Limited
 * @copyright Copyright (c) Magetop Software Private Limited (https://magetop.com)
 * @license   https://store.magetop.com/license.html
 */

namespace Magetop\Helpdesk\Controller\TicketsThread;

use Magetop\Helpdesk\Controller\AbstractController;

/**
 * AddCollaborater class
 */
class AddCollaborater extends AbstractController
{
    /** @var \Magento\Framework\View\Result\PageFactory */
    private $resultPageFactory;

    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    private $jsonResultFactory;

    /** @var \Helpdesk\Model\TicketManagerCustomer */
    private $ticketManagerCustomer;

    /**
     * __construct function
     *
     * @param \Magento\Backend\App\Action\Context                 $context
     * @param \Magento\Framework\View\Result\PageFactory          $resultPageFactory
     * @param \Magetop\Helpdesk\Model\TicketManagerCustomer $ticketManagerCustomer
     * @param \Magento\Framework\Controller\Result\JsonFactory    $jsonResultFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magetop\Helpdesk\Model\TicketManagerCustomer $ticketManagerCustomer,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->ticketManagerCustomer = $ticketManagerCustomer;
        $this->jsonResultFactory = $jsonResultFactory;
        parent::__construct($context, $resultPageFactory);
    }

    public function execute()
    {
        $result = $this->jsonResultFactory->create();
        $successCount = 0;
        $errorCount = 0;
        $post = $this->getRequest()->getParams();
        if ((isset($post['ticketId']) &&
            !empty($post['ticketId'])) &&
            (isset($post['email']) &&
            !empty($post['email']))
        ) {
                $response = $this->ticketManagerCustomer->addCollaborater(
                    $post['ticketId'],
                    $post['email']
                );
                return $result->setData($response);
        }
    }

    /*
     * Check permission via ACL resource
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magetop_Helpdesk::tickets');
    }
}
