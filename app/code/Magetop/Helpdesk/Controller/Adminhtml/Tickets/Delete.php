<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Helpdesk
 * @author    Magetop Software Private Limited
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://magetop.com)
 * @license   https://store.magetop.com/license.html
 */

namespace Magetop\Helpdesk\Controller\Adminhtml\Tickets;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Delete class
 */
class Delete extends \Magento\Backend\App\Action
{

    /** @var \Magetop\Helpdesk\Model\TicketManager */
    private $ticketManager;

    /**
     * __construct function
     *
     * @param \Magento\Backend\App\Action\Context         $context
     * @param \Magetop\Helpdesk\Model\TicketManager $ticketManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magetop\Helpdesk\Model\TicketManager $ticketManager
    ) {
    
        parent::__construct($context);
        $this->ticketManager = $ticketManager;
    }

    public function execute()
    {
        $successCount = 0;
        $errorCount = 0;
        $post = $this->getRequest()->getParams();
        if (isset($post['id']) && !empty($post['id'])) {
            $response = $this->ticketManager->deleteTicket($post['id']);
            if ($response['response']) {
                $successCount++;
            } else {
                $errorCount++;
            }
            if ($successCount) {
                $this->messageManager->addSuccess(__("Success ! Ticket(s) removed successfully."));
            }
            if ($errorCount) {
                $this->messageManager->addError(__("Error ! %1 Tickets not removed successfully."));
            }
        }
    }

    /*
     * Check permission via ACL resource
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magetop_Helpdesk::tickets');
    }
}
