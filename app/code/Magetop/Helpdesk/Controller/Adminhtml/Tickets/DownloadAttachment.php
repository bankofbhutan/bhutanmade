<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Helpdesk
 * @author    Magetop Software Private Limited
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://magetop.com)
 * @license   https://store.magetop.com/license.html
 */

namespace Magetop\Helpdesk\Controller\Adminhtml\Tickets;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * DownloadAttachment class
 */
class DownloadAttachment extends \Magento\Backend\App\Action
{
    /** @var \Magetop\Helpdesk\Model\TicketManager */
    private $ticketManager;
    
    /** @var \Magento\Framework\App\Response\Http\FileFactory */
    private $fileFactory;
    
    /** @var \Magento\Framework\Controller\Result\RawFactory */
    private $resultRawFactory;

    /**
     * __construct function
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magetop\Helpdesk\Model\TicketManager $ticketManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magetop\Helpdesk\Model\TicketManager $ticketManager
    ) {
    
        parent::__construct($context);
        $this->ticketManager        = $ticketManager;
        $this->resultRawFactory      = $resultRawFactory;
    }

    public function execute()
    {
        $attachmenId = $this->getRequest()->getParam('attachment_id');
        $name = $this->getRequest()->getParam('name');
        $file = $this->ticketManager->downloadAttachment($attachmenId);
        header('Content-Disposition: attachment; filename="'.$name.'"');
        header('Content-Type: '.$file['info']['content_type']);
        header('Content-Length: ' . strlen($file['response']));
        header('Connection: close');
        $resultRaw = $this->resultRawFactory->create();
        $resultRaw->setContents($file['response']); //set content for download file here
        return $resultRaw;
    }

    /*
     * Check permission via ACL resource
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magetop_Helpdesk::tickets');
    }
}
