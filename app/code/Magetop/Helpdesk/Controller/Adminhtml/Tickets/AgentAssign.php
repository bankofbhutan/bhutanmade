<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Helpdesk
 * @author    Magetop Software Private Limited
 * @copyright Copyright (c) 2010-2017 Magetop Software Private Limited (https://magetop.com)
 * @license   https://store.magetop.com/license.html
 */

namespace Magetop\Helpdesk\Controller\Adminhtml\Tickets;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * AgentAssign class
 */
class AgentAssign extends \Magento\Backend\App\Action
{

    /** @var \Helpdesk\Model\TicketManager */
    private $ticketManager;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonResultFactory;

    /**
     * __construct function
     *
     * @param \Magento\Backend\App\Action\Context              $context
     * @param \Magetop\Helpdesk\Model\TicketManager      $ticketManager
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magetop\Helpdesk\Model\TicketManager $ticketManager,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    ) {
        parent::__construct($context);
        $this->ticketManager = $ticketManager;
        $this->jsonResultFactory = $jsonResultFactory;
    }

    public function execute()
    {
        $result = $this->jsonResultFactory->create();
        $ticketId = $this->getRequest()->getParam('ticketid');
        $agentId = $this->getRequest()->getParam('agentId');
        $tickets = $this->ticketManager->assignAgentToTicket($ticketId, $agentId);
        return $result->setData($tickets);
    }
    
    /*
     * Check permission via ACL resource
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magetop_Helpdesk::tickets');
    }
}
