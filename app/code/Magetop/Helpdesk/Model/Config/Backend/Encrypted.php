<?php
/**
 * Magetop Software.
 *
 * @category  Magetop
 * @package   Magetop_Helpdesk
 * @author    Magetop Software Private Limited
 * @copyright Copyright (c) Magetop Software Private Limited (https://magetop.com)
 * @license   https://store.magetop.com/license.html
 */

namespace Magetop\Helpdesk\Model\Config\Backend;

class Encrypted extends \Magento\Config\Model\Config\Backend\Encrypted
{
    /**
     * Encrypt value before saving
     *
     * @return void
     */
    public function beforeSave()
    {
        $uvdeskToken = $this->getFieldsetDataValue("uvdesk_accesstoken");
        $uvdeskDomainName = $this->getFieldsetDataValue("uvdesk_companydomain");
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $ticketManager = $objectManager->create('Magetop\Helpdesk\Model\TicketManager');
        $bool = $ticketManager->checkCredentials($uvdeskToken, $uvdeskDomainName);
        if ($bool) {
            $isAdminLevelAccess = $ticketManager->getTheDetailOfEnteredTokenAgent($uvdeskToken, $uvdeskDomainName);
            if ($isAdminLevelAccess) {
                parent::beforeSave();
            } else {
                $this->_dataSaveAllowed = false;
                throw new \Magento\Framework\Exception\LocalizedException(
                    __(
                        'Entered Credentials do not have admin level access.'.
                        'Please save admin level access credentials'
                    )
                );
            }
        } else {
            $this->_dataSaveAllowed = false;
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid Credentials.'));
        }
    }
}
