<?php
/**
 * @Author      : Kien
 * @package     Marketplace_Multiple_Table_Rate_Shipping
 * @copyright   Copyright (c) 2020 MAGETOP (http://www.magetop.com)
 * @terms  http://www.magetop.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
namespace Magetop\SellerTableRateShipping\Model\Checkout;

class ShippingInformationManagementPlugin
{
    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        $tableRate = $extAttributes->getTableRateShipping();
        if($tableRate){
            $data = explode('split',$tableRate);
            \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Catalog\Model\Session')->setTableRateShippingPrice($data[0]);
            $quote = $this->quoteRepository->getActive($cartId);
            $quote->setTableRateShipping(substr($data[1],0,-1));
        }
    }
}