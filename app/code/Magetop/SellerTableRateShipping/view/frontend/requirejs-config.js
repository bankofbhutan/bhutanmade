/**
 * Copyright � 2015 Scommerce Mage. All rights reserved.
 * See COPYING.txt for license details.
 */
 
var config = {
    map: {
        '*': {
            "Magento_Checkout/js/view/shipping" : "Magetop_SellerTableRateShipping/js/view/shipping",
            "Magento_Checkout/js/model/shipping-save-processor/default" : "Magetop_SellerTableRateShipping/js/shipping-save-processor-default-override",
            "Magento_Checkout/js/view/shipping-address/address-renderer/default" : "Magetop_SellerTableRateShipping/js/view/shipping-address/address-renderer/default",
            "Magento_Checkout/js/model/shipping-rate-processor/new-address" : "Magetop_SellerTableRateShipping/js/model/shipping-rate-processor/new-address",
            "Magento_Checkout/js/model/shipping-rate-processor/customer-address" : "Magetop_SellerTableRateShipping/js/model/shipping-rate-processor/customer-address",
        }
    }
};