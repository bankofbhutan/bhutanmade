<?php

namespace CloudBhutan\BOBWorldlineInr\Observer;

use Magento\Framework\Event\ObserverInterface;

class OrderEmailCancel implements ObserverInterface
{
    /**
     * Disallow sending the order email after placing the order
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @var  \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        if ($this->hasBOBWorldlineInrPaymentMethod($quote)) {
            $order->setCanSendNewEmailFlag(false);
        }
    }

    /**
     * Checks if quote that is being submitted has BOBWorldline payment method
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    protected function hasBOBWorldlineInrPaymentMethod($quote)
    {
        return $quote->getPayment()->getMethodInstance()->getCode() == \CloudBhutan\BOBWorldlineInr\Model\BOBWorldlineInr::CODE;
    }
}