<?php

namespace CloudBhutan\BOBWorldlineInr\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckoutSubmitAllAfter implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();

        if ($quote->getPayment()->getMethodInstance()->getCode() == \CloudBhutan\BOBWorldlineInr\Model\BOBWorldlineInr::CODE) {
            $quote->setReservedOrderId(null);
            $quote->setIsActive(true)->save();
        }
    }
}
