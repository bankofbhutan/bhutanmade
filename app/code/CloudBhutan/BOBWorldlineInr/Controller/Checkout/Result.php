<?php

namespace CloudBhutan\BOBWorldlineInr\Controller\Checkout;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use CloudBhutan\BOBWorldlineInr\Helper\OrderProcessor;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Result
 */
class Result extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{
    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Config
     */
    protected $config;

    /**
     * @var WorldlineSecurity
     */
    protected $worldlineSecurity;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Helper\ResponseHandler
     */
    protected $responseHandler;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Logger\Logger
     */
    protected $logger;

    /**
     * @var OrderProcessor
     */
    protected $orderProcessor;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \CloudBhutan\BOBWorldlineInr\Helper\ResponseHandler $responseHandler
     * @param OrderProcessor $orderProcessor
     * @param \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \CloudBhutan\BOBWorldlineInr\Helper\ResponseHandler $responseHandler,
        \CloudBhutan\BOBWorldlineInr\Helper\OrderProcessor $orderProcessor,
        \CloudBhutan\BOBWorldlineInr\Model\Hmac\WorldlineSecurity $worldlineSecurity,
        \CloudBhutan\BOBWorldlineInr\Model\Config $config,
        \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
    ) {
        parent::__construct($context);
        
        $this->config = $config;
        $this->worldlineSecurity = $worldlineSecurity;
        $this->responseHandler = $responseHandler;
        $this->orderProcessor = $orderProcessor;
        $this->logger = $logger;
    }

    /** 
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request 
    ): ?InvalidRequestException {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        $response = $this->getRequest()->getParams();
        $result = null;

        try {
            $result = $this->responseHandler->handleResponse($response);
        } catch (\Exception $e) {
            $this->logger->error('Exception while processing response', [$e->getMessage(), $e->getTrace()]);
        }

        if ($this->isPaymentSuccessful($response)) {
            if ($result === true) {
                return $this->_redirect('checkout/onepage/success');
            } else if (is_array($result)) {
                foreach ($result as $error) {
                    $this->messageManager->addErrorMessage($error);
                }

                $this->logger->error('Error while validating response payload', $result);
            }
        } else if (!$this->isPaymentSuccessful($response)) {
            $this->cancelOrderAndRestoreQuote($response);
            return $this->redirectToCart();
        }

        $this->messageManager->addNoticeMessage(
            __('It seems that something went wrong. Please don\'t hesitate to contact us if any errors occurred during the payment process')
        );

        return $this->_redirect('checkout/onepage/failure');
    }

    /**
     * Check whether the payment was successful from the response data
     *
     * @param $response
     * @return bool
     */
    public function isPaymentSuccessful($response)
    {

        /**
            64A703D25A9E44E81B39C298A0A571590C5D46DC1AD7308F63C058EC9ED4ED735C54A1CA5F1BEED12DC3417D8A96FF32EC7E9C9BF8F916A76F7859B64770B5DD30F5BA5A69174C01E65C94A4D820D8F00D337C1B26720A6B4F4EA8191DF3A028660A1679083B2CB9BC6ECD3A4A3FE5D5E7EAD75F195CFED92652D43E67485421978727C71B5EFA14B0D1C42CA3C7A8A9228583B23B99817F240EC0F689DF1F0A45738CB8BC55086A6E4FBDEFA5A95551040A1310702A74C7FB9C83D7D4B2FC53EFFF5C2A18D826E8082DFFA21A9712D6

        */

        $merchantResponse = $response['merchantResponse'];

        $decryptData = $this->worldlineSecurity->decryptValue($merchantResponse, $this->config->getSecretKey());

        $resArray = explode('|',$decryptData);

        $success = false;

        // if (isset($response['payment_state'])) {
        //     if ($response['payment_state'] == 'settled' || $response['payment_state'] == 'authorized') {
        //         $success = true;
        //     } else if ($response['payment_state'] == 'cancelled'
        //         || $response['payment_state'] == 'waiting_for_3ds_response'
        //         || $response['payment_state'] == 'failed') {
        //         $success =  false;
        //     }
        // } else if (isset($response['cancel'])) { //from IFrame
        //     $success =  false;
        // }

        // return $success;

        if( $resArray[10] == 'S'){
            $success = true;
        }
        else{
            $success = false;
        }

        return $success;
    }

    /**
     * Cancel the order, add a notice
     *
     * @param $response
     * @return void
     */
    public function cancelOrderAndRestoreQuote($response)
    {
        try {
            $merchantResponse = $response['merchantResponse'];

            $decryptData = $this->worldlineSecurity->decryptValue($merchantResponse, $this->config->getSecretKey());

            $resArray = explode('|',$decryptData);


            $this->orderProcessor->cancelOrderAndRestoreQuote();
            $this->messageManager->addNoticeMessage(__('Order has been cancelled. %1', $resArray[11]));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
            $this->logger->error('Error while cancelling the order', [$response, $e->getMessage(), $e->getTrace()]);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Unable to cancel order'));
            $this->logger->error('Error while cancelling the order', [$response, $e->getMessage(), $e->getTrace()]);
        }
    }

    /**
     * Redirect to cart page
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function redirectToCart() {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('checkout/cart');
    }
}