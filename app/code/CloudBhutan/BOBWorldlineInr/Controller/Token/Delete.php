<?php

namespace CloudBhutan\BOBWorldlineInr\Controller\Token;

class Delete extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \CloudBhutan\BOBWorldline\Api\Data\TokenRepositoryInterface
     */
    protected $tokenRepository;

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Token\TokenManagement
     */
    protected $tokenManager;

    /**
     * @var \CloudBhutan\BOBWorldline\Logger\Logger
     */
    protected $logger;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \CloudBhutan\BOBWorldline\Api\Data\TokenRepositoryInterface $tokenRepository
     * @param \CloudBhutan\BOBWorldline\Model\Token\TokenManagement $tokenManager
     * @param \CloudBhutan\BOBWorldline\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \CloudBhutan\BOBWorldlineInr\Api\Data\TokenRepositoryInterface $tokenRepository,
        \CloudBhutan\BOBWorldlineInr\Model\Token\TokenManagement $tokenManager,
        \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->tokenRepository = $tokenRepository;
        $this->tokenManager = $tokenManager;
        $this->logger = $logger;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $id = $this->_request->getParam('id');
        $result = $this->resultJsonFactory->create();
        $token  = null;

        try {
            $token = $this->tokenRepository->getById($id);
        } catch (\Exception $e) {
            $this->logger->error('Exception while retrieving token', [$e->getMessage(), $e->getTrace()]);
            return $result->setData(['success' => false]);
        }

        if ($this->tokenManager->isCustomersToken($token)) {
            try {
                $this->tokenRepository->delete($token);
                $result->setData(['success' => true]);
            } catch (\Exception $e) {
                $this->logger->error('Exception while deleting token', [$e->getMessage(), $e->getTrace()]);
                $result->setData(['success' => false]);
            }
        } else {
            $result->setData(['success' => false]);
        }

        return $result;
    }

}