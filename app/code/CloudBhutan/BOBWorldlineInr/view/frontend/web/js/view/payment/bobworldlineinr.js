define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'bobworldlineinr',
                component: 'CloudBhutan_BOBWorldlineInr/js/view/payment/method-renderer/bobworldlineinr'
            }
        );

        return Component.extend({});
    }
);