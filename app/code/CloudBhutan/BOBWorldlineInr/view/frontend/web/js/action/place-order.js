define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'Magento_Customer/js/model/customer',
        'mage/storage',
        'Magento_Checkout/js/model/place-order',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader'
    ],
    function ($, quote, urlBuilder, customer, storage, placeOrderService, errorProcessor, fullScreenLoader) {
        'use strict';

        return function (paymentData, messageContainer, tokenData) {
            var serviceUrl, payload;

            // keep compatible with M2.*
            var agreementForm = $('.payment-method._active form[data-role=checkout-agreements]'),
                agreementData = agreementForm.serializeArray(),
                agreementIds = [];

            agreementData.forEach(function(item) {
                agreementIds.push(item.value);
            });

            paymentData.extension_attributes = {agreement_ids: agreementIds};
            // ========

            payload = {
                cartId: quote.getQuoteId(),
                tokenAgreement: tokenData.isAgree,
                billingAddress: quote.billingAddress(),
                paymentMethod: paymentData
            };

            if (customer.isLoggedIn()) {
                serviceUrl = urlBuilder.createUrl('/bobworldlineinr/payment-adapter/mine', {});
                payload.tokenId = tokenData.tokenId;
            } else {
                serviceUrl = urlBuilder.createUrl('/bobworldlineinr/payment-adapter/:quoteId', {
                    quoteId: quote.getQuoteId()
                });
                payload.email = quote.guestEmail;
            }

            // keep compatible with M2.*
            return storage.post(
                serviceUrl,
                JSON.stringify(payload)
            ).fail(
                function (response) {
                    errorProcessor.process(response, messageContainer);
                    fullScreenLoader.stopLoader();
                }
            );
        };
    }
);
