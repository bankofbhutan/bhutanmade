define(
    [
        'jquery',
        'Magento_Checkout/js/model/full-screen-loader'
    ],
    function ($, fullScreenLoader) {
        'use strict';

        function createForm(data) {
            var endpoint = window.checkoutConfig.payment.bobworldlineinr.endpoint;

            var form = "<form id='bobworldlineinr-transaction-data-form'  action='" + endpoint + "' method='post'";
            if (window.checkoutConfig.payment.bobworldline.isIFrame) {
                form += "target='bobworldlineinr-payment-container'";
            }
            form += ">";

            $.each(data, function(name, value) {
                form += "<input type='text' name='" + name + "' value='" + value + "' hidden />";
            });
            form += "<input type='submit' class='bobworldlineinr-submit-button' value='Submit' hidden /></form>";

            return form;
        }

        return function (data) {
            $("#bobworldlineinr-transaction-data-form").remove();
            var form = createForm(data);
            $('body').append(form);
            $(".bobworldlineinr-submit-button").click();

            fullScreenLoader.stopLoader();
            if (window.checkoutConfig.payment.bobworldlineinr.isIFrame) {
                $('#bobworldlineinr-popup').modal('openModal');
            }
            return true;
        };
    }
);
