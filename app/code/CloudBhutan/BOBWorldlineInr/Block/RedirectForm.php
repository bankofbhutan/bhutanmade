<?php

namespace CloudBhutan\BOBWorldlineInr\Block;


class RedirectForm extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \CloudBhutan\BOBWorldlineInr\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \CloudBhutan\BOBWorldlineInr\Model\Config $config,
        array $data = []
    ) {
        $this->config = $config;

        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isIFrame()
    {
        return $this->config->getFormType() ===  \CloudBhutan\BOBWorldlineInr\Model\Adminhtml\Source\FormType::FORM_TYPE_IFRAME;
    }
}