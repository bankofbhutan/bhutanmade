<?php

namespace CloudBhutan\BOBWorldlineInr\Block;

class PaymentForm extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \CloudBhutan\BOBWorldlineInr\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \CloudBhutan\BOBWorldlineInr\Model\Config $config,
        array $data = []
    ) {
        $this->config = $config;

        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\View\Element\Template
     */
    protected function _prepareLayout()
    {
        $this->addChild(
            'iframe',
            'CloudBhutan\BOBWorldlineInr\Block\IFrame',
            ['template' => 'CloudBhutan_BOBWorldlineInr::iframe.phtml']
        );

        $this->addChild(
            'redirect-form',
            'CloudBhutan\BOBWorldlineInr\Block\RedirectForm',
            ['template' => 'CloudBhutan_BOBWorldlineInr::redirect-form.phtml']
        );

        return parent::_prepareLayout();
    }

    /**
     * @return bool
     */
    public function isIFrame()
    {
        return $this->config->getFormType() ===  \CloudBhutan\BOBWorldlineInr\Model\Adminhtml\Source\FormType::FORM_TYPE_IFRAME;
    }
}