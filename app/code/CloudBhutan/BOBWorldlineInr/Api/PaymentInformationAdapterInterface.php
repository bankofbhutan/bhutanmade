<?php

namespace CloudBhutan\BOBWorldlineInr\Api;

/**
 * Interface for managing quote payment information
 * @api
 */
interface PaymentInformationAdapterInterface
{
    /**
     * @param string $cartId
     * @param string $tokenAgreement
     * @param int $tokenId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
     * @return string
     */
    public function adaptPlaceOrder(
        $cartId,
        $tokenAgreement,
        $tokenId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    );
}
