<?php

namespace CloudBhutan\BOBWorldlineInr\Model\Form;

abstract class AbstractData
{
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \CloudBhutan\BOBWorldlineInr\Model\Config $config
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \CloudBhutan\BOBWorldlineInr\Model\Config $config
    ) {
        $this->orderRepository = $orderRepository;
        $this->config = $config;
    }

    /**
     * @param int $orderID
     * @return mixed
     */
    public abstract function getFields($orderID);
}