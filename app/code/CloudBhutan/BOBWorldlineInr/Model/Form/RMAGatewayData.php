<?php

namespace CloudBhutan\RMAGateway\Model\Form;

use Magento\Framework\Exception\LocalizedException;
use CloudBhutan\RMAGateway\Model\Hmac\RMAGatewaySecurity;

class RMAGatewayData extends AbstractData
{
    
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;


    /**
     * @var \Magento\Directory\Api\CountryInformationAcquirerInterface
     */
    protected $country;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var \CloudBhutan\RMAGateway\Model\Hmac\RMAGatewaySecurity
     */
    protected $rmagatewaySecurity;

    /**
     * @var \CloudBhutan\RMAGateway\Logger\Logger
     */
    protected $logger;

    /**
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Directory\Api\CountryInformationAcquirerInterface $country
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \CloudBhutan\RMAGateway\Model\Config $config
     * @param \CloudBhutan\RMAGateway\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Directory\Api\CountryInformationAcquirerInterface $country,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \CloudBhutan\RMAGateway\Model\Config $config,
        \CloudBhutan\RMAGateway\Logger\Logger $logger,
        \CloudBhutan\RMAGateway\Model\Hmac\RMAGatewaySecurity $rmagatewaySecurity
    ) {
        $this->country = $country;
        $this->remoteAddress = $remoteAddress;
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->rmagatewaySecurity = $rmagatewaySecurity;
        $this->urlBuilder = $urlBuilder;

        parent::__construct($orderRepository, $config);
    }

    /**
     * @param int $orderId
     * @return array
     * @throws LocalizedException
     */
    public function getFields($orderId)
    {

        //$merchantRequest = $this->rmagatewaySecurity->encryptValue($this->getWorldlineFields($orderId), $this->config->getSecretKey());

        $order = $this->orderRepository->get($orderId);

        //RMA Merchant Account
        $mId = $this->config->getMerchantId();
        $bfs_msgType = 'AR';
        date_default_timezone_set('Asia/Thimphu');

        $bfs_benfTxnTime = date('YmdHis');;
        $bfs_orderNo = $order->getRealOrderId();
        $bfs_benfId = $this->config->getMerchantId();
        $bfs_benfBankCode = '01';
        $bfs_txnCurrency = 'BTN';
        $bfs_txnAmount = number_format($order->getBaseGrandTotal() * 1, 0, '.', '');
        $bfs_remitterEmail = 'bhutanpost62@gmail.com';
        
        $bfs_paymentDesc = "Payment for GNH Corner";
        $bfs_version = '1.0';

        $checkSumData = $bfs_benfBankCode.'|'.$bfs_benfId.'|'.$bfs_benfTxnTime.'|'.$bfs_msgType.'|'.$bfs_orderNo.'|'.$bfs_paymentDesc.'|'.$bfs_remitterEmail.'|'.$bfs_txnAmount.'|'.$bfs_txnCurrency.'|'.$bfs_version;

        $bfs_checkSum = $this->rmagatewaySecurity->generateCheckSum($checkSumData);


        $data = [
            'bfs_msgType' => $bfs_msgType,
            'bfs_benfTxnTime' => $bfs_benfTxnTime,
            'bfs_orderNo' => $bfs_orderNo,
            'bfs_benfId' => $bfs_benfId,
            'bfs_benfBankCode' => $bfs_benfBankCode,
            'bfs_txnCurrency' => $bfs_txnCurrency,
            'bfs_txnAmount' => $bfs_txnAmount,
            'bfs_remitterEmail' => $bfs_remitterEmail,
            'bfs_checkSum' => $bfs_checkSum,
            'bfs_paymentDesc' => $bfs_paymentDesc,
            'bfs_version' => $bfs_version
        ];

        foreach ($data as $field => $value) {
            if (empty($value)) {
                throw new LocalizedException(__('Required field %1 is missing', $field));
            }
        }
        return $data;
    }

    /** worldine fields
    *
    */
    public function getWorldlineFields($orderId){

        $order = $this->orderRepository->get($orderId);

        
        $mId = $this->config->getMerchantId();

        $trnAmt = number_format($order->getBaseGrandTotal() * 100, 0, '.', '');
        $orderId = $order->getRealOrderId();
        $trnCurrency = $this->config->getCurrency();
        $trnRemarks =  '' ;
        $meTransReqType = 'S';
        $recurrPeriod = '';
        $recurrDay = '';
        $noOfRecurring = '';
        $responseUrl = $this->urlBuilder->getRouteUrl('rmagateway/checkout/result');
        $addField1 = '';
        $addField2 = '';
        $addField3 = '';
        $addField4 = '';
        $addField5 = '';
        $addField6 = '';
        $addField7 = '';
        $addField8 = '';
        $addField9 = 'NA';
        $addField10 = 'NA';
        
        return $mId  . "|" . $orderId  . "|" . $trnAmt  . "|" . 
            $trnCurrency  . "|" . $trnRemarks  . "|" . $meTransReqType  . "|" . 
            $recurrPeriod  . "|" . $recurrDay  . "|" . $noOfRecurring  . "|" . 
            $responseUrl  . "|" . $addField1  . "|" . $addField2  . "|" . 
            $addField3  . "|" . $addField4  . "|" . $addField5  . "|" . 
            $addField6  . "|" . $addField7  . "|" . $addField8  . "|" . 
            $addField9  . "|" . $addField10 ;

    }

    /**
     * Remove single quotes from field values to get a correct HMAC
     *
     * @param array $fields
     * @return mixed
     */
    protected function sanitizeFields($fields) {
        foreach ($fields as $field => $value) {
            $fields[$field] = str_replace("'", "", $value);
        }

        return $fields;
    }


    /**
     *
     * @param
     *          $inputVal
     * @param
     *          $secureKey
     * @return string
     */
    protected function encryptValue($inputVal, $secureKey) {
        $key = '';
        for($i = 0; $i < strlen ( $secureKey ) - 1; $i += 2) {
            $key .= chr ( hexdec ( $secureKey [$i] . $secureKey [$i + 1] ) );
        }
        
        $block = @mcrypt_get_block_size ( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB );
        $pad = $block - (strlen ( $inputVal ) % $block);
        $inputVal .= str_repeat ( chr ( $pad ), $pad );
        
        $encrypted_text = bin2hex ( @mcrypt_encrypt ( MCRYPT_RIJNDAEL_128, $key, $inputVal, MCRYPT_MODE_ECB ) );
        
        return $encrypted_text;
    }
    /**
     *
     * @param
     *          $inputVal
     * @param
     *          $secureKey
     * @return string
     */
    protected function decryptValue($inputVal, $secureKey) {
        $key = '';
        for($i = 0; $i < strlen ( $secureKey ) - 1; $i += 2) {
            
            $key .= chr ( hexdec ( $secureKey [$i] . $secureKey [$i + 1] ) );
        }
        
        $encblock = '';
        for($i = 0; $i < strlen ( $inputVal ) - 1; $i += 2) {
            $encblock .= chr ( hexdec ( $inputVal [$i] . $inputVal [$i + 1] ) );
        }
        
        $decrypted_text = @mcrypt_decrypt ( MCRYPT_RIJNDAEL_128, $key, $encblock, MCRYPT_MODE_ECB );
        
        return $decrypted_text;
    }
}
