<?php

namespace CloudBhutan\BOBWorldlineInr\Model\Form;

use Magento\Framework\Exception\LocalizedException;

class OrderData extends AbstractData
{
    /**
     * @var \Magento\Directory\Api\CountryInformationAcquirerInterface
     */
    protected $country;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var \CloudBhutan\BOBWorldline\Logger\Logger
     */
    protected $logger;

    /**
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Directory\Api\CountryInformationAcquirerInterface $country
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \CloudBhutan\BOBWorldlineInr\Model\Config $config
     * @param \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Directory\Api\CountryInformationAcquirerInterface $country,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \CloudBhutan\BOBWorldlineInr\Model\Config $config,
        \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
    ) {
        $this->country = $country;
        $this->remoteAddress = $remoteAddress;
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;

        parent::__construct($orderRepository, $config);
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getFields($orderId)
    {
        $order = $this->orderRepository->get($orderId);
        $requiredFields = $this->getRequiredFields($order);
        $optionalFields = $this->getOptionalFields($order);

        return array_merge($requiredFields, $optionalFields);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return array
     * @throws LocalizedException
     */
    protected function getRequiredFields(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $requiredFields = [
            'email' => $order->getCustomerEmail(),
            'amount' => $order->getBaseGrandTotal(),
            'order_reference' => $order->getIncrementId(),
            'user_ip' => $this->remoteAddress->getRemoteAddress(),
            'timestamp' => $this->resolveTimeStamp($order->getEntityId())
        ];

        foreach ($requiredFields as $field => $value) {
            if (empty($value)) {
                throw new LocalizedException(__('Required %1 field is missing', $field));
            }
        }

        return $this->sanitizeFields($requiredFields);
    }

    /**
     * @param int $orderId
     * @return false|int
     */
    protected function resolveTimeStamp($orderId)
    {
        return strtotime($this->orderRepository->get($orderId)->getCreatedAt());
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return array
     */
    protected function getOptionalFields(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        return array_merge($this->getBillingAddressFields($order), $this->getShippingAddressFields($order));
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return array
     */
    protected function getBillingAddressFields(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $fields = [
            'billing_address' =>  implode(' - ', $order->getBillingAddress()->getStreet()),
            'billing_country' => $this->country
                ->getCountryInfo($order->getBillingAddress()->getCountryId())->getTwoLetterAbbreviation(),
            'billing_city' => $order->getBillingAddress()->getCity(),
            'billing_postcode' => $order->getBillingAddress()->getPostcode()
        ];

        return $this->sanitizeFields($fields);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return array
     */
    protected function getShippingAddressFields(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $shippingAddress = null;

        try {
            $shippingAddress = $this->quoteRepository->get($order->getQuoteId())->getShippingAddress();
        } catch (\Exception $e) {
            $this->logger
                 ->error("Error while retrieving quote's shipping address", [$e->getMessage(), $e->getTrace()]);
        }

        $fields = [];

        if ($shippingAddress && !$shippingAddress->isEmpty()) {
            try {
                $fields = [
                    'delivery_address' => implode(' - ', $shippingAddress->getStreet()),
                    'delivery_country' => $this->country
                        ->getCountryInfo($shippingAddress->getCountryId())->getTwoLetterAbbreviation(),
                    'delivery_city' => $shippingAddress->getCity(),
                    'delivery_postcode' => $shippingAddress->getPostcode()
                ];
            } catch (\Exception $e) {
                $this->logger
                     ->error("Error while retrieving quote's shipping address", [$e->getMessage(), $e->getTrace()]);
            }
        }

        return $this->sanitizeFields($fields);
    }

    /**
     * Remove single quotes from field values to get a correct HMAC
     *
     * @param array $fields
     * @return mixed
     */
    protected function sanitizeFields($fields) {
        foreach ($fields as $field => $value) {
            $fields[$field] = str_replace("'", "", $value);
        }

        return $fields;
    }
}
