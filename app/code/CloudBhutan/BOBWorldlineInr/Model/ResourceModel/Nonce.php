<?php

namespace CloudBhutan\BOBWorldlineInr\Model\ResourceModel;

class Nonce extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Primary key auto increment flag
     *
     * @var bool
     */
    protected $_isPkAutoIncrement = false;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('cloudbhutan_bobworldlineinr_nonce', 'nonce');
    }
}
