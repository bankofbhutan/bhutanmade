<?php

namespace CloudBhutan\BOBWorldlineInr\Model\ResourceModel\Nonce;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'nonce';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\BOBWorldlineInr\Model\Nonce\Nonce', 'CloudBhutan\BOBWorldlineInr\Model\ResourceModel\Nonce');
    }
}
