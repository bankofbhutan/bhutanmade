<?php

namespace CloudBhutan\BOBWorldlineInr\Model\ResourceModel\Token;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\BOBWorldlineInr\Model\Token\Token', 'CloudBhutan\BOBWorldlineInr\Model\ResourceModel\Token');
    }
    // @codingStandardsIgnoreEnd
}
