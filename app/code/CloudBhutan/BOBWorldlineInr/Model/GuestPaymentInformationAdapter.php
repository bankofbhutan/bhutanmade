<?php

namespace CloudBhutan\BOBWorldlineInr\Model;

use Magento\Framework\Exception\LocalizedException;

class GuestPaymentInformationAdapter implements \CloudBhutan\BOBWorldlineInr\Api\GuestPaymentInformationAdapterInterface
{
    /**
     * @var \Magento\Checkout\Api\GuestPaymentInformationManagementInterface
     */
    protected $guestPaymentInformationManagement;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Form\FormDataBuilder
     */
    protected $formDataBuilder;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Logger\Logger
     */
    protected $logger;

    /**
     * @param \Magento\Checkout\Api\GuestPaymentInformationManagementInterface $guestPaymentInformationManagement
     * @param \CloudBhutan\BOBWorldlineInr\Model\Form\FormDataBuilder $formDataBuilder
     * @param \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Checkout\Api\GuestPaymentInformationManagementInterface $guestPaymentInformationManagement,
        \CloudBhutan\BOBWorldlineInr\Model\Form\FormDataBuilder $formDataBuilder,
        \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
    ) {
        $this->guestPaymentInformationManagement = $guestPaymentInformationManagement;
        $this->formDataBuilder = $formDataBuilder;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function adaptPlaceOrder(
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        try {
            $orderId = $this->guestPaymentInformationManagement->savePaymentInformationAndPlaceOrder(
                $cartId, $email, $paymentMethod, $billingAddress
            );

            return json_encode($this->formDataBuilder->getFields($orderId));
        } catch (\Exception $e) {
            $this->logger->error('An error occured while placing the order', [$e->getMessage(), $e->getTrace()]);
            throw new LocalizedException(__($e->getMessage()));
        }
    }
}
