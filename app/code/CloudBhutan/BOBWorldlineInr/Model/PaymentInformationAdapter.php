<?php

namespace CloudBhutan\BOBWorldlineInr\Model;

use Magento\Framework\Exception\LocalizedException;

class PaymentInformationAdapter implements \CloudBhutan\BOBWorldlineInr\Api\PaymentInformationAdapterInterface
{
    /**
     * @var \Magento\Checkout\Api\PaymentInformationManagementInterface
     */
    protected $paymentInformationManagement;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Form\FormDataBuilder
     */
    protected $formDataBuilder;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Token\TokenManagement
     */
    protected $tokenManager;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Logger\Logger
     */
    protected $logger;

    /**
     * @param \Magento\Checkout\Api\PaymentInformationManagementInterface $paymentInformationManagement
     * @param \CloudBhutan\BOBWorldlineInr\Model\Form\FormDataBuilder $formDataBuilder
     * @param \CloudBhutan\BOBWorldlineInr\Model\Token\TokenManagement $tokenManager
     * @param \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $paymentInformationManagement,
        \CloudBhutan\BOBWorldlineInr\Model\Form\FormDataBuilder $formDataBuilder,
        \CloudBhutan\BOBWorldlineInr\Model\Token\TokenManagement $tokenManager,
        \CloudBhutan\BOBWorldlineInr\Logger\Logger $logger
    ) {
        $this->paymentInformationManagement = $paymentInformationManagement;
        $this->formDataBuilder = $formDataBuilder;
        $this->tokenManager = $tokenManager;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function adaptPlaceOrder(
        $cartId,
        $tokenAgreement,
        $tokenId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        try {
            $orderId = $this->paymentInformationManagement->savePaymentInformationAndPlaceOrder(
                $cartId, $paymentMethod, $billingAddress
            );

            $tokenData = $this->tokenManager->getTokenData($tokenAgreement, $tokenId);
            return json_encode($this->formDataBuilder->getFields($orderId, $tokenData));
        } catch (\Exception $e) {
            $this->logger->error('An error occured while placing the order', [$e->getMessage(), $e->getTrace()]);
            throw new LocalizedException(__($e->getMessage()));
        }
    }
}
