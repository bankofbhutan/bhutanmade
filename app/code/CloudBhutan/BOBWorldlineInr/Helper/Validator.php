<?php

namespace CloudBhutan\BOBWorldlineInr\Helper;

class Validator extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * This is a minor security flaw, but this check is needed to bypass redirection to failure page
     * if Worldline sent the same nonce back twice. This situation occurs when the customer
     * clicks on the 'back' button when Worldline had already sent data back to merchant.
     */
    const NONCE_AGE = 60;

    
    const RESPONSE_AGE = 600;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $orderInterface;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Config
     */
    protected $config;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Hmac\HmacManagement
     */
    protected $hmac;

    /**
     * @var \CloudBhutan\BOBWorldlineInr\Model\Nonce\NonceManagement
     */
    protected $nonce;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Sales\Api\Data\OrderInterface $orderInterface
     * @param \CloudBhutan\BOBWorldlineInr\Model\Config $config
     * @param \CloudBhutan\BOBWorldlineInr\Model\Hmac\HmacManagement $hmac
     * @param \CloudBhutan\BOBWorldlineInr\Model\Nonce\NonceManagement $nonce
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface,
        \CloudBhutan\BOBWorldlineInr\Model\Config $config,
        \CloudBhutan\BOBWorldlineInr\Model\Hmac\HmacManagement $hmac,
        \CloudBhutan\BOBWorldlineInr\Model\Nonce\NonceManagement $nonce
    ) {
        $this->orderInterface = $orderInterface;
        $this->config = $config;
        $this->hmac = $hmac;
        $this->nonce = $nonce;

        parent::__construct($context);
    }

    /**
     * @param array $data
     * @return bool|array
     */
    public function validate(array $data)
    {
        $this->errors = [];

        $this
            //->validateHmac($data)
            //->validateTimestamp($data)
            //->validateNonce($data)
            ->validateOrderReference($data);
            //->validateUsername($data);

        if (!empty($this->errors)) {
            return $this->errors;
        }

        return true;
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function validateUsername(array $data)
    {
        if (!isset($data['api_username']) || $data['api_username'] !== $this->config->getApiUser()) {
            $this->errors[] = __('Response username is invalid');
        }

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function validateOrderReference(array $data)
    {
        try {
            $this->orderInterface->loadByIncrementId($data[1]);
        } catch (\Exception $e) {
            $this->errors[] = __('Order does not exist');
        }

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function validateNonce(array $data)
    {
        if (!isset($data['nonce']) || !$this->nonce->isNonceUnique($data['nonce'], self::NONCE_AGE)) {
            $this->errors[] = __('Response nonce is invalid');
        }

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function validateTimestamp(array $data)
    {
        $now = time();

        if (!isset($data['timestamp']) ||
            ($data['timestamp'] > $now) || ($data['timestamp'] < ($now - self::RESPONSE_AGE))
        ) {
            $this->errors[] = __('Response is outdated');
        }

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function validateHmac(array $data)
    {
        if (!isset($data['hmac_fields']) || !$this->hmac->isResponseHmacValid($data)) {
            $this->errors[] = __('Response HMAC is invalid');
        }

        return $this;
    }
}
