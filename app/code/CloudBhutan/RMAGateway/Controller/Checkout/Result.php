<?php

namespace CloudBhutan\RMAGateway\Controller\Checkout;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use CloudBhutan\RMAGateway\Helper\OrderProcessor;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Result
 */
class Result extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{
    /**
     * @var \CloudBhutan\RMAGateway\Model\Config
     */
    protected $config;

    /**
     * @var RMAGatewaySecurity
     */
    protected $rmagatewaySecurity;

    /**
     * @var \CloudBhutan\RMAGateway\Helper\ResponseHandler
     */
    protected $responseHandler;

    /**
     * @var \CloudBhutan\RMAGateway\Logger\Logger
     */
    protected $logger;

    /**
     * @var OrderProcessor
     */
    protected $orderProcessor;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \CloudBhutan\RMAGateway\Helper\ResponseHandler $responseHandler
     * @param OrderProcessor $orderProcessor
     * @param \CloudBhutan\RMAGateway\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \CloudBhutan\RMAGateway\Helper\ResponseHandler $responseHandler,
        \CloudBhutan\RMAGateway\Helper\OrderProcessor $orderProcessor,
        \CloudBhutan\RMAGateway\Model\Hmac\RMAGatewaySecurity $rmagatewaySecurity,
        \CloudBhutan\RMAGateway\Model\Config $config,
        \CloudBhutan\RMAGateway\Logger\Logger $logger
    ) {
        parent::__construct($context);
        
        $this->config = $config;
        $this->rmagatewaySecurity = $rmagatewaySecurity;
        $this->responseHandler = $responseHandler;
        $this->orderProcessor = $orderProcessor;
        $this->logger = $logger;
    }

    /** 
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request 
    ): ?InvalidRequestException {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        $response = $this->getRequest()->getParams();
        $result = null;

        try {
            $result = $this->responseHandler->handleResponse($response);
        } catch (\Exception $e) {
            $this->logger->error('Exception while processing response', [$e->getMessage(), $e->getTrace()]);
        }

        if ($this->isPaymentSuccessful($response)) {
            if ($result === true) {
                $this->messageManager->addSuccessMessage(__("Your Order is successful"));
                return $this->_redirect('checkout/onepage/success');
            } else if (is_array($result)) {
                foreach ($result as $error) {
                    $this->messageManager->addErrorMessage($error);
                }

                $this->logger->error('Error while validating response payload', $result);
            }
        } else if (!$this->isPaymentSuccessful($response)) {
            $this->cancelOrderAndRestoreQuote($response);
            return $this->redirectToCart();
        }

        $this->messageManager->addNoticeMessage(
            __('It seems that something went wrong. Please don\'t hesitate to contact us if any errors occurred during the payment process')
        );

        return $this->_redirect('checkout/onepage/failure');
    }

    /**
     * Check whether the payment was successful from the response data
     *
     * @param $response
     * @return bool
     */
    public function isPaymentSuccessful($response)
    {

       

        $bfs_bfsTxnId = $response['bfs_bfsTxnId'];
        $bfs_benfTxnTime = $response['bfs_benfTxnTime'];

        //$decryptData = $this->worldlineSecurity->decryptValue($merchantResponse, $this->config->getSecretKey());

       // $resArray = explode('|',$decryptData);

       // $success = false;

        // if (isset($response['payment_state'])) {
        //     if ($response['payment_state'] == 'settled' || $response['payment_state'] == 'authorized') {
        //         $success = true;
        //     } else if ($response['payment_state'] == 'cancelled'
        //         || $response['payment_state'] == 'waiting_for_3ds_response'
        //         || $response['payment_state'] == 'failed') {
        //         $success =  false;
        //     }
        // } else if (isset($response['cancel'])) { //from IFrame
        //     $success =  false;
        // }

        // return $success;

        if($response['bfs_debitAuthCode'] == '00'){
            $success = true;
        }
        else{
            $success = false;
        }
        return $success;
    }

    /**
     * Cancel the order, add a notice
     *
     * @param $response
     * @return void
     */
    public function cancelOrderAndRestoreQuote($response)
    {
        try {
            // $merchantResponse = $response['merchantResponse'];

            // $decryptData = $this->worldlineSecurity->decryptValue($merchantResponse, $this->config->getSecretKey());

            // $resArray = explode('|',$decryptData);


            $this->orderProcessor->cancelOrderAndRestoreQuote();

            $responseMessage = [
                '00' => 'Approved',
                '03' => 'Invalid Beneficiary',
                '05' => 'Beneficiary Account Closed',
                '12' => 'Invalid Transaction',
                '13' => 'Invalid Amount',
                '14' => 'Invalid Remitter Account',
                '20' => 'Invalid Response',
                '30' => 'Transaction Not Supported or Format Error',
                '45' => 'Duplicate Beneficary Order Number',
                '47' => 'Invallid currency',
                '48' => 'Trasaction Limit Exceeded',
                '51' => 'Insufficient Funds',
                '53' => 'No Savings Account',
                '57' => 'Transaction not permitted',
                '61' => 'Withdrawal Limit Exceeded',
                '65' => 'Withdrawal Frequency Exceeded',
                '76' => 'Transaction Not Found',
                '78' => 'Decryption Failed',
                '80' => 'Buyer Cancel Transaction',
                '84' => 'Invalid Transaction Type',
                '85' => 'Internal Error At Bank System',
                'UC' => 'Transaction Cancelled By Customer',
                'UN' => 'Unknown Error',
                'TO' => 'Session Timeout at BFS Secure Entry Page',
                'NF' => 'Transaction Not Found',
                'IM' => 'Invalid Message'
            ];

            $responseCode = $response['bfs_debitAuthCode'];

            $this->messageManager->addNoticeMessage(__('Order has been cancelled. %1', $responseCode[$responseCode]));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
            $this->logger->error('Error while cancelling the order', [$response, $e->getMessage(), $e->getTrace()]);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Unable to cancel order'));
            $this->logger->error('Error while cancelling the order', [$response, $e->getMessage(), $e->getTrace()]);
        }
    }

    /**
     * Redirect to cart page
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function redirectToCart() {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('checkout/cart');
    }
}