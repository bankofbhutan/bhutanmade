<?php

namespace CloudBhutan\RMAGateway\Model\ResourceModel;

class Token extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('cloudbhutan_rmagateway_token', 'id');
    }
}
