<?php

namespace CloudBhutan\RMAGateway\Model\ResourceModel\Nonce;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'nonce';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\RMAGateway\Model\Nonce\Nonce', 'CloudBhutan\RMAGateway\Model\ResourceModel\Nonce');
    }
}
