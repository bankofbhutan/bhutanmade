<?php

namespace CloudBhutan\RMAGateway\Model\ResourceModel\Token;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\RMAGateway\Model\Token\Token', 'CloudBhutan\RMAGateway\Model\ResourceModel\Token');
    }
    // @codingStandardsIgnoreEnd
}
