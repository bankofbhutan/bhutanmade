<?php

namespace CloudBhutan\RMAGateway\Model\Nonce;

use Magento\Framework\Api\SortOrder;

class NonceRepository implements \CloudBhutan\RMAGateway\Api\Data\NonceRepositoryInterface
{
    /**
     * @var \Magento\Framework\Api\SearchResultsInterfaceFactory
     */
    protected $searchResultFactory;

    /**
     * @var \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce
     */
    protected $resourceModel;

    /**
     * @param \Magento\Framework\Api\SearchResultsInterfaceFactory $searchResultFactory
     * @param \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce\CollectionFactory $collectionFactory
     * @param \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce $resourceModel
     */
    public function __construct(
        \Magento\Framework\Api\SearchResultsInterfaceFactory $searchResultFactory,
        \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce\CollectionFactory $collectionFactory,
        \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce $resourceModel
    ) {
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionFactory = $collectionFactory;
        $this->resourceModel = $resourceModel;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\CloudBhutan\RMAGateway\Api\Data\NonceInterface $nonce)
    {
        $this->resourceModel->save($nonce);
        return $nonce;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce\Collection $collection */
        $collection = $this->collectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }

        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders === null) {
            $sortOrders = [];
        }
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        foreach ($sortOrders as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
            );
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $nonces = [];
        /** @var \CloudBhutan\RMAGateway\Model\Nonce\Nonce $nonceModel */
        foreach ($collection->getItems() as $nonceModel) {
            $nonces[] = $nonceModel->getData();
        }

        /** @var \Magento\Framework\Api\SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($nonces);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce\Collection $collection
     * @return void
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \CloudBhutan\RMAGateway\Model\ResourceModel\Nonce\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

}