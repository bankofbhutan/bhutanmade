<?php
/**
 * 
 * Copyrights: Worldline e-payment
 * @ MadeBy : Mindgate Software Solutions 
 *  
 */
namespace CloudBhutan\RMAGateway\Model\Hmac;

use phpseclib\Crypt\RSA;

class RMAGatewaySecurity {
	
	/**
     * @var \CloudBhutan\RMAGateway\Model\Config
     */
    protected $config;

    /**
     * @param \CloudBhutan\RMAGateway\Model\Config $config
     */
    public function __construct(
        \CloudBhutan\RMAGateway\Model\Config $config
    ) {
        $this->config = $config;
    }

	/**
	 *
	 * @param
	 *        	$inputVal
	 * @param
	 *        	$secureKey
	 * @return string
	 */
	function encryptValue($inputVal, $secureKey) {
		$key = '';
		for($i = 0; $i < strlen ( $secureKey ) - 1; $i += 2) {
			$key .= chr ( hexdec ( $secureKey [$i] . $secureKey [$i + 1] ) );
		}
		
		$block = @mcrypt_get_block_size ( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB );
		$pad = $block - (strlen ( $inputVal ) % $block);
		$inputVal .= str_repeat ( chr ( $pad ), $pad );
		
		$encrypted_text = bin2hex ( @mcrypt_encrypt ( MCRYPT_RIJNDAEL_128, $key, $inputVal, MCRYPT_MODE_ECB ) );
		
		return $encrypted_text;
	}
	/**
	 *
	 * @param
	 *        	$inputVal
	 * @param
	 *        	$secureKey
	 * @return string
	 */
	function decryptValue($inputVal, $secureKey) {
		$key = '';
		for($i = 0; $i < strlen ( $secureKey ) - 1; $i += 2) {
			
			$key .= chr ( hexdec ( $secureKey [$i] . $secureKey [$i + 1] ) );
		}
		
		$encblock = '';
		for($i = 0; $i < strlen ( $inputVal ) - 1; $i += 2) {
			$encblock .= chr ( hexdec ( $inputVal [$i] . $inputVal [$i + 1] ) );
		}
		
		$decrypted_text = @mcrypt_decrypt ( MCRYPT_RIJNDAEL_128, $key, $encblock, MCRYPT_MODE_ECB );
		
		return $decrypted_text;
	}

	/**
	 *
	 * @param
	 *        	$checkSumString
	 * @return string
	 */
	function generateCheckSum($checkSumString){

		$signature = null;

		$algo = "SHA1";
		$priv_key = file_get_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'privatekey.key');
		$pkeyid = openssl_get_privatekey($priv_key);

		$data_base64_encode = base64_encode($checkSumString);

		openssl_sign($checkSumString, $signature, $pkeyid, $algo);
		openssl_free_key($pkeyid);

		$hex = bin2hex($signature);
		return strtoupper($hex);
	}

}
