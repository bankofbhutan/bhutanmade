<?php

namespace CloudBhutan\RMAGateway\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;

class Environment implements ArrayInterface
{
    const ENVIRONMENT_PRODUCTION = 'production';
    const ENVIRONMENT_TEST = 'test';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::ENVIRONMENT_TEST,
                'label' => ucfirst(self::ENVIRONMENT_TEST),
            ],
            [
                'value' => self::ENVIRONMENT_PRODUCTION,
                'label' => ucfirst(self::ENVIRONMENT_PRODUCTION)
            ]
        ];
    }
}
