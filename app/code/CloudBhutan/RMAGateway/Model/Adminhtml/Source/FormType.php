<?php

namespace CloudBhutan\RMAGateway\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;

class FormType implements ArrayInterface
{
    const FORM_TYPE_IFRAME = 'iframe';
    const FORM_TYPE_REDIRECT = 'redirect';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::FORM_TYPE_IFRAME,
                'label' => 'Embedded payment form (HTML iFrame)',
            ],
            [
                'value' => self::FORM_TYPE_REDIRECT,
                'label' => 'Redirect to payment page'
            ]
        ];
    }
}
