<?php

namespace CloudBhutan\RMAGateway\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;

class TokenSecurity implements ArrayInterface
{
    const TOKEN_SECURITY_NONE = 'none';
    const TOKEN_SECURITY_CVC = 'cvc';
    const TOKEN_SECURITY_3DS = '3ds';
    const TOKEN_SECURITY_CVC_3DS = 'cvc_3ds';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::TOKEN_SECURITY_NONE,
                'label' => ucfirst(self::TOKEN_SECURITY_NONE) . ' (recommended)',
            ],
            [
                'value' => self::TOKEN_SECURITY_CVC,
                'label' => strtoupper(self::TOKEN_SECURITY_CVC),
            ],
            [
                'value' => self::TOKEN_SECURITY_3DS,
                'label' => strtoupper(self::TOKEN_SECURITY_3DS),
            ],
            [
                'value' => self::TOKEN_SECURITY_CVC_3DS,
                'label' => 'CVC with 3DS authentication',
            ]
        ];
    }
}
