<?php

namespace CloudBhutan\RMAGateway\Model\Form;

class FormDataBuilder
{
    /**
     * @var \CloudBhutan\RMAGateway\Model\Config
     */
    protected $config;

    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    protected $locale;

    /**
     * @var \CloudBhutan\RMAGateway\Model\Hmac\HmacManagement
     */
    protected $hmac;

    /**
     * @var ApiData
     */
    protected $apiData;

    /**
     * @var OrderData
     */
    protected $orderData;

    /**
     * @var TokenData
     */
    protected $tokenData;

    /**
     * @var TokenData
     */
    protected $rmagatewayData;

    /**
     * @param \Magento\Framework\Locale\Resolver $locale
     * @param \CloudBhutan\RMAGateway\Model\Config $config
     * @param \CloudBhutan\RMAGateway\Model\Hmac\HmacManagement $hmac
     * @param ApiData $apiData
     * @param OrderData $orderData
     * @param TokenData $tokenData
     */
    public function __construct(
        \Magento\Framework\Locale\Resolver $locale,
        \CloudBhutan\RMAGateway\Model\Config $config,
        \CloudBhutan\RMAGateway\Model\Hmac\HmacManagement $hmac,
        ApiData $apiData,
        OrderData $orderData,
        TokenData $tokenData,
        RMAGatewayData $rmagatewayData
    ) {
        $this->locale = $locale;
        $this->config = $config;
        $this->hmac = $hmac;
        $this->apiData = $apiData;
        $this->orderData = $orderData;
        $this->tokenData = $tokenData;
        $this->rmagatewayData = $rmagatewayData;
    }

    /**
     * @param int $orderId
     * @param null|array $tokenData
     * @return array
     */
    public function getFields($orderId, $tokenData = null)
    {
        if ($this->config->getCcTokenEnabled() && $tokenData != null) {
            $tokenFields = $this->tokenData->getFields($tokenData);
        } else {
            $tokenFields = [];
        }

        $data = 
            array_merge(
                $this->apiData->getFields($orderId),
                $this->orderData->getFields($orderId),
                $this->rmagatewayData->getFields($orderId),
                $tokenFields
        );
        // $data = $this->hmac->appendHmacFields(
        //         $this->apiData->getFields($orderId),
        //         $this->orderData->getFields($orderId),
        //         $tokenFields
        //     )
        // );

        // $order = $this->orderData->getFields($orderId);

        // //$config = Mage::getSingleton('worldline/config');
    
        // $mId = $config->getMerchantId();
        // $trnAmt = number_format($order->getBaseGrandTotal() * 100, 0, '.', '');
        // $orderId = $order->getRealOrderId();
        // $trnCurrency = $config->getCurrency();
        // $trnRemarks =  '' ;
        // $meTransReqType = 'S';
        // $recurrPeriod = '';
        // $recurrDay = '';
        // $noOfRecurring = '';
        // $responseUrl = $config->getResponseUrl();
        // $addField1 = '';
        // $addField2 = '';
        // $addField3 = '';
        // $addField4 = '';
        // $addField5 = '';
        // $addField6 = '';
        // $addField7 = '';
        // $addField8 = '';
        // $addField9 = 'NA';
        // $addField10 = 'NA';
        
        // return $mId  . "|" . $orderId  . "|" . $trnAmt  . "|" . 
        //     $trnCurrency  . "|" . $trnRemarks  . "|" . $meTransReqType  . "|" . 
        //     $recurrPeriod  . "|" . $recurrDay  . "|" . $noOfRecurring  . "|" . 
        //     $responseUrl  . "|" . $addField1  . "|" . $addField2  . "|" . 
        //     $addField3  . "|" . $addField4  . "|" . $addField5  . "|" . 
        //     $addField6  . "|" . $addField7  . "|" . $addField8  . "|" . 
        //     $addField9  . "|" . $addField10 ;


        $data['locale'] = $this->resolveLocale();
        return $data;
    }

    /**
     * @return string
     */
    protected function resolveLocale()
    {
        $locale = $this->locale->getLocale();
        $locale = substr($locale, 0, strpos($locale, '_'));

        if (!in_array($locale, $this->config->getSupportedLocales())) {
            $locale =  $this->config->getDefaultLocale();
        }

        return $locale;
    }
}
