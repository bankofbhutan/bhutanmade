<?php

namespace CloudBhutan\RMAGateway\Model\Form;

use Magento\Framework\Exception\LocalizedException;

class TokenData extends AbstractData
{
    /**
     * @var \CloudBhutan\RMAGateway\Api\Data\TokenRepositoryInterface
     */
    protected $tokenRepository;

    /**
     * @var \CloudBhutan\RMAGateway\Model\Token\TokenManagement
     */
    protected $tokenManager;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \CloudBhutan\RMAGateway\Model\Config $config
     * @param \CloudBhutan\RMAGateway\Api\Data\TokenRepositoryInterface $tokenRepository
     * @param \CloudBhutan\RMAGateway\Model\Token\TokenManagement $tokenManager
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \CloudBhutan\RMAGateway\Model\Config $config,
        \CloudBhutan\RMAGateway\Api\Data\TokenRepositoryInterface $tokenRepository,
        \CloudBhutan\RMAGateway\Model\Token\TokenManagement $tokenManager
    ) {
        $this->tokenRepository = $tokenRepository;
        $this->tokenManager = $tokenManager;

        parent::__construct($orderRepository, $config);
    }

    /**
     * @param array $tokenData
     * @return array
     */
    public function getFields($tokenData)
    {
        if (!isset($tokenData['request_cc_token']) || !isset($tokenData['token_id'])
            || $tokenData['request_cc_token'] != 1) {
            return [];
        }

        if ($this->isNewTokenRequest($tokenData)) {
            return $this->getNewTokenFields();
        }

        return $this->getSavedTokenFields($tokenData);
    }

    /**
     * @param array $tokenData
     * @return bool
     */
    protected function isNewTokenRequest($tokenData)
    {
        return $tokenData['token_id'] == \CloudBhutan\RMAGateway\Model\Config::NEW_CARD_ID;
    }

    /**
     * @return array
     */
    protected function getNewTokenFields()
    {
        return [
            'request_cc_token' => 1
        ];
    }

    /**
     * @param array $tokenData
     * @return array
     * @throws LocalizedException
     */
    protected function getSavedTokenFields($tokenData)
    {
        $token = $this->tokenRepository->getById($tokenData['token_id']);
        $data = [];

        if ($this->tokenManager->isCustomersToken($token)) {
            $data['cc_token'] = $token->getCcToken();

            if ($this->config->getTokenSecurity()
                != \CloudBhutan\RMAGateway\Model\Adminhtml\Source\TokenSecurity::TOKEN_SECURITY_NONE) {
                $data['token_security'] = $this->config->getTokenSecurity();
            }
        } else {
            throw new LocalizedException(__('This card does not exist'));
        }

        return $data;
    }

}
