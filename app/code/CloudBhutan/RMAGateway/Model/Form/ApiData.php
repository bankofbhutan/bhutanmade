<?php

namespace CloudBhutan\RMAGateway\Model\Form;

use Magento\Framework\Exception\LocalizedException;

class ApiData extends AbstractData
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \CloudBhutan\RMAGateway\Model\Nonce\NonceManagement
     */
    protected $nonce;

    /**
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \CloudBhutan\RMAGateway\Model\Config $config
     * @param \CloudBhutan\RMAGateway\Model\Nonce\NonceManagement $nonce
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \CloudBhutan\RMAGateway\Model\Config $config,
        \CloudBhutan\RMAGateway\Model\Nonce\NonceManagement $nonce
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->nonce = $nonce;

        parent::__construct($orderRepository, $config);
    }

    /**
     * @param int $orderId
     * @return array
     * @throws LocalizedException
     */
    public function getFields($orderId)
    {
        $data = [
            //'api_username' => $this->config->getApiUser(),
            'account_id' => $this->config->getProcessingAccount(),
            'nonce' => $this->nonce->generateNonce(),
            'callback_url' => $this->urlBuilder->getRouteUrl('rmagateway/checkout/result'),
            'customer_url' => $this->urlBuilder->getRouteUrl('rmagateway/checkout/result'),
            'transaction_type' => 'charge'
        ];

        foreach ($data as $field => $value) {
            if (empty($value)) {
                throw new LocalizedException(__('Required field %1 is missing', $field));
            }
        }
        if ($this->config->getFormType() == \CloudBhutan\RMAGateway\Model\Adminhtml\Source\FormType::FORM_TYPE_IFRAME) {
            $data['skin_name'] = $this->config->getSkinName();
        }

        return $data;
    }

}
