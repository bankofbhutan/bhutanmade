<?php

namespace CloudBhutan\RMAGateway\Model\Form;

abstract class AbstractData
{
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \CloudBhutan\RMAGateway\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \CloudBhutan\RMAGateway\Model\Config $config
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \CloudBhutan\RMAGateway\Model\Config $config
    ) {
        $this->orderRepository = $orderRepository;
        $this->config = $config;
    }

    /**
     * @param int $orderID
     * @return mixed
     */
    public abstract function getFields($orderID);
}