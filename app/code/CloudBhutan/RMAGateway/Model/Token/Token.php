<?php

namespace CloudBhutan\RMAGateway\Model\Token;

use CloudBhutan\RMAGateway\Api\Data\TokenInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

class Token extends AbstractExtensibleModel implements TokenInterface, IdentityInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'cloudbhutan_rmagateway_token';

    /**
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'cloudbhutan_rmagateway_token';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getObject() in this case
     *
     * @var string
     */
    protected $_eventObject = 'token';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\RMAGateway\Model\ResourceModel\Token');
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * {@inheritDoc}
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function getCcToken()
    {
        return $this->getData(self::CC_TOKEN);
    }

    /**
     * {@inheritDoc}
     */
    public function getCcLastFourDigits()
    {
        return $this->getData(self::CC_LAST_FOUR_DIGITS);
    }

    /**
     * {@inheritDoc}
     */
    public function getCcMonth()
    {
        return $this->getData(self::CC_MONTH);
    }

    /**
     * {@inheritDoc}
     */
    public function getCcYear()
    {
        return $this->getData(self::CC_YEAR);
    }

    /**
     * {@inheritDoc}
     */
    public function getCcHolderName()
    {
        return $this->getData(self::CC_HOLDER_NAME);
    }

    /**
     * {@inheritDoc}
     */
    public function getCcType()
    {
        return $this->getData(self::CC_TYPE);
    }

    /**
     * {@inheritDoc}
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * {@inheritDoc}
     */
    public function setCcToken($token)
    {
        return $this->setData(self::CC_TOKEN, $token);
    }

    /**
     * {@inheritDoc}
     */
    public function setCcLastFourDigits($digits)
    {
        return $this->setData(self::CC_LAST_FOUR_DIGITS, $digits);
    }

    /**
     * {@inheritDoc}
     */
    public function setCcMonth($month)
    {
        return $this->setData(self::CC_MONTH, $month);
    }

    /**
     * {@inheritDoc}
     */
    public function setCcYear($year)
    {
        return $this->setData(self::CC_YEAR, $year);
    }

    /**
     * {@inheritDoc}
     */
    public function setCcHolderName($name)
    {
        return $this->setData(self::CC_HOLDER_NAME, $name);
    }

    /**
     * {@inheritDoc}
     */
    public function setCcType($type)
    {
        return $this->setData(self::CC_TYPE, $type);
    }
}