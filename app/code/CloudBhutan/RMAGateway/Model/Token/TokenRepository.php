<?php

namespace CloudBhutan\RMAGateway\Model\Token;

use Magento\Framework\Api\SortOrder;

class TokenRepository implements \CloudBhutan\RMAGateway\Api\Data\TokenRepositoryInterface
{
    /**
     * @var \Magento\Framework\Api\SearchResultsInterfaceFactory
     */
    protected $searchResultFactory;

    /**
     * @var \CloudBhutan\RMAGateway\Api\Data\TokenInterfaceFactory
     */
    protected $tokenFactory;

    /**
     * @var \CloudBhutan\RMAGateway\Model\ResourceModel\Token\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \CloudBhutan\RMAGateway\Model\ResourceModel\Token
     */
    protected $resourceModel;

    /**
     * @param \Magento\Framework\Api\SearchResultsInterfaceFactory $searchResultFactory
     * @param \CloudBhutan\RMAGateway\Api\Data\TokenInterfaceFactory $tokenFactory
     * @param \CloudBhutan\RMAGateway\Model\ResourceModel\Token\CollectionFactory $collectionFactory
     * @param \CloudBhutan\RMAGateway\Model\ResourceModel\Token $resourceModel
     */
    public function __construct(
        \Magento\Framework\Api\SearchResultsInterfaceFactory $searchResultFactory,
        \CloudBhutan\RMAGateway\Api\Data\TokenInterfaceFactory $tokenFactory,
        \CloudBhutan\RMAGateway\Model\ResourceModel\Token\CollectionFactory $collectionFactory,
        \CloudBhutan\RMAGateway\Model\ResourceModel\Token $resourceModel
    ) {
        $this->searchResultFactory = $searchResultFactory;
        $this->tokenFactory = $tokenFactory;
        $this->collectionFactory = $collectionFactory;
        $this->resourceModel = $resourceModel;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\CloudBhutan\RMAGateway\Api\Data\TokenInterface $token)
    {
        $tokenId = $token->getId();

        if ($tokenId) {
            $existingToken = $this->load($tokenId);
            $mergedData = array_merge($existingToken->getData(), $token->getData());
            $token->setData($mergedData);
        }

        $this->resourceModel->save($token);
        return $token;
    }

    /**
     * {@inheritdoc)
     */
    public function delete(\CloudBhutan\RMAGateway\Api\Data\TokenInterface $token)
    {
        $this->resourceModel->delete($token);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($tokenId)
    {
        $token = $this->tokenFactory->create()->load($tokenId);

        if (!$token->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException();
        }

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \CloudBhutan\RMAGateway\Model\ResourceModel\Token\Collection $collection */
        $collection = $this->collectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }

        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders === null) {
            $sortOrders = [];
        }
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        foreach ($sortOrders as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
            );
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $tokens = [];
        /** @var \CloudBhutan\RMAGateway\Model\Token\Token $tokenModel */
        foreach ($collection->getItems() as $tokenModel) {
            $tokens[] = $tokenModel->getData();
        }

        $searchResults = $this->searchResultFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($tokens);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \CloudBhutan\RMAGateway\Model\ResourceModel\Token\Collection $collection
     * @return void
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \CloudBhutan\RMAGateway\Model\ResourceModel\Token\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
}