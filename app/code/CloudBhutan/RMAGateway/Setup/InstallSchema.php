<?php

namespace CloudBhutan\RMAGateway\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
            $installer->getTable('cloudbhutan_rmagateway_nonce')
        )->addColumn(
            'nonce',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Nonce'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addIndex(
            $installer->getIdxName(
                'cloudbhutan_rmagateway_nonce',
                ['nonce'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['nonce'],
            ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'Received nonces'
        );
        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable(
            $installer->getTable('cloudbhutan_rmagateway_token')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Token ID'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true],
            'Customer ID'
        )->addColumn(
            'cc_token',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            25,
            ['nullable' => false],
            'CC Token'
        )->addColumn(
            'cc_last_four_digits',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            4,
            [],
            'Last four digits of the card number'
        )->addColumn(
            'cc_month',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            2,
            [],
            'Card expiration month (mm)'
        )->addColumn(
            'cc_year',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            4,
            [],
            'Card expiration year (YYYY)'
        )->addColumn(
            'cc_holder_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Cardholder name'
        )->addColumn(
            'cc_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            11,
            [],
            'Card type'
        )->addIndex(
            $installer->getIdxName('cloudbhutan_rmagateway_token', ['customer_id']),
            ['customer_id']
        )->addForeignKey(
            $installer->getFkName('cloudbhutan_rmagateway_token', 'customer_id', 'customer_entity', 'entity_id'),
            'customer_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'CC tokens for RMAGateway'
        );

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
