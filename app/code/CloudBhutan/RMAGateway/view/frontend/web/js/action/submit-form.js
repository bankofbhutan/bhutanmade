define(
    [
        'jquery',
        'Magento_Checkout/js/model/full-screen-loader'
    ],
    function ($, fullScreenLoader) {
        'use strict';

        function createForm(data) {
            var endpoint = window.checkoutConfig.payment.rmagateway.endpoint;

            var form = "<form id='rmagateway-transaction-data-form'  action='" + endpoint + "' method='post'";
            if (window.checkoutConfig.payment.rmagateway.isIFrame) {
                form += "target='rmagateway-payment-container'";
            }
            form += ">";

            $.each(data, function(name, value) {
                form += "<input type='text' name='" + name + "' value='" + value + "' hidden />";
            });
            form += "<input type='submit' class='rmagateway-submit-button' value='Submit' hidden /></form>";

            return form;
        }

        return function (data) {
            $("#rmagateway-transaction-data-form").remove();
            var form = createForm(data);
            $('body').append(form);
            $(".rmagateway-submit-button").click();

            fullScreenLoader.stopLoader();
            if (window.checkoutConfig.payment.rmagateway.isIFrame) {
                $('#rmagateway-popup').modal('openModal');
            }
            return true;
        };
    }
);
