define(
    [
        'ko',
        'jquery',
        'mage/translate',
        'Magento_Ui/js/modal/alert',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/full-screen-loader',
        'CloudBhutan_RMAGateway/js/action/submit-form',
        'CloudBhutan_RMAGateway/js/action/place-order',
        'CloudBhutan_RMAGateway/js/model/token-manager',
        'mage/mage'
    ],
    function (
        ko,
        $,
        $t,
        magentoAlert,
        Component,
        additionalValidators,
        quote,
        fullScreenLoader,
        submitFormAction,
        placeOrderAction,
        tokenManager
    ) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false,
            tokenManager: tokenManager(),

            selectedCard: ko.observable(null),
            isAgree: ko.observable(window.checkoutConfig.payment.rmagateway.customer.cards.length > 0),
            isTokenEnabled: window.checkoutConfig.payment.rmagateway.isTokenEnabled,
            isDisplayLogos: window.checkoutConfig.payment.rmagateway.isDisplayLogos,
            isLoggedIn: window.checkoutConfig.payment.rmagateway.customer.isLoggedIn,
            hasTokens: window.checkoutConfig.payment.rmagateway.customer.cards.length > 0,
            loginUrl: window.checkoutConfig.payment.rmagateway.customerUrl,

            defaults: {
                template: 'CloudBhutan_RMAGateway/payment/rmagateway'
            },
            validate: function () {
                var billingAddress = quote.billingAddress();

                if (!billingAddress.city || !billingAddress.countryId || !billingAddress.postcode) {
                    magentoAlert({
                        title: $t('Error'),
                        content: $t('Your session has expired'),
                        actions: {
                            always: function () {
                                var url = window.checkoutConfig.checkoutUrl;
                                window.location.replace(url);
                            }
                        }
                    });
                    return false;
                }
                return true;
            },
            placeOrder: function (data, event) {

                var self = this;
                var placeOrder;

                if (event) {
                    event.preventDefault();
                }

                fullScreenLoader.startLoader();
                $('body').trigger('processStart');

                if (self.validate() && additionalValidators.validate()) {
                    placeOrder = placeOrderAction(self.getData(), self.messageContainer, self.getTokenData());
                    placeOrder.fail(function () {
                        fullScreenLoader.stopLoader();
                    }).done(self.afterPlaceOrder.bind(self));
                }

                fullScreenLoader.stopLoader();
                return true;
            },
            afterPlaceOrder: function (data) {
                return submitFormAction($.parseJSON(data));
            },
            getTokenData: function() {
                var tokenId = null;

                if (this.selectedCard() != null) {
                    tokenId = this.selectedCard();
                } else {
                    tokenId = window.checkoutConfig.payment.rmagateway.newCardId;
                }

                return {
                    tokenId: tokenId,
                    isAgree: this.isAgree()
                };
            },
            getCode: function() {
                return 'rmagateway';
            },
            getLogos: function() {
                return window.checkoutConfig.payment.rmagateway.logos;
            },
            deleteToken: function(id) {
                fullScreenLoader.startLoader();
                $.ajax({
                    url: window.checkoutConfig.payment.rmagateway.deleteUrl + 'id/' + id.id,
                    type: 'get',
                    success: function(data) {
                        if (data.success) {
                            $('.card-token-' + id.id).remove();
                        }
                    }
                });
                fullScreenLoader.stopLoader();
            }
        });
    }
);