define([
    'ko',
    'jquery'
], function(ko, $) {
    "use strict";

    $.widget("cloudbhutan.tokenmanager", {
        customerData: window.checkoutConfig.payment.rmagateway.customer,
        selectedCard: ko.observable(null),
        isAgree: ko.observable(false),
        _create: function() {

        },
        getCards: function() {
            return this.customerData.cards;
        }
    });

    return $.cloudbhutan.tokenmanager;
});