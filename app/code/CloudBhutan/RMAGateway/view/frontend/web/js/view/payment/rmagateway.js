define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'rmagateway',
                component: 'CloudBhutan_RMAGateway/js/view/payment/method-renderer/rmagateway'
            }
        );

        return Component.extend({});
    }
);