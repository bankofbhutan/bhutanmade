<?php

namespace CloudBhutan\RMAGateway\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckoutSubmitAllAfter implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();

        if ($quote->getPayment()->getMethodInstance()->getCode() == \CloudBhutan\RMAGateway\Model\RMAGateway::CODE) {
            $quote->setReservedOrderId(null);
            $quote->setIsActive(true)->save();
        }
    }
}
