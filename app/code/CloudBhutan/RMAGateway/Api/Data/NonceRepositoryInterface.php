<?php

namespace CloudBhutan\RMAGateway\Api\Data;


interface NonceRepositoryInterface
{
    /**
     * Create or update a nonce.
     *
     * @param \CloudBhutan\RMAGateway\Api\Data\NonceInterface $nonce
     * @return \CloudBhutan\RMAGateway\Api\Data\NonceInterface
     */
    public function save(\CloudBhutan\RMAGateway\Api\Data\NonceInterface $nonce);

    /**
     * Get a list of nonces matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface;
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}