<?php

namespace CloudBhutan\RMAGateway\Block;

class PaymentForm extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \CloudBhutan\RMAGateway\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \CloudBhutan\RMAGateway\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \CloudBhutan\RMAGateway\Model\Config $config,
        array $data = []
    ) {
        $this->config = $config;

        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\View\Element\Template
     */
    protected function _prepareLayout()
    {
        $this->addChild(
            'iframe',
            'CloudBhutan\RMAGateway\Block\IFrame',
            ['template' => 'CloudBhutan_RMAGateway::iframe.phtml']
        );

        $this->addChild(
            'redirect-form',
            'CloudBhutan\RMAGateway\Block\RedirectForm',
            ['template' => 'CloudBhutan_RMAGateway::redirect-form.phtml']
        );

        return parent::_prepareLayout();
    }

    /**
     * @return bool
     */
    public function isIFrame()
    {
        return $this->config->getFormType() ===  \CloudBhutan\RMAGateway\Model\Adminhtml\Source\FormType::FORM_TYPE_IFRAME;
    }
}