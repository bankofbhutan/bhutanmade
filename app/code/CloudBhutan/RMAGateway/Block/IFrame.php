<?php

namespace CloudBhutan\RMAGateway\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class IFrame
 */
class IFrame extends \Magento\Framework\View\Element\Template
{
    /**
     * Get the cancelling url
     *
     * @return string
     */
    public function getCancelUrl()
    {
        return $this->getUrl('rmagateway/checkout/result', ['cancel' => '1']);
    }
}