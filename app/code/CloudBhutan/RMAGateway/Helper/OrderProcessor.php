<?php

namespace CloudBhutan\RMAGateway\Helper;

use Magento\Framework\Exception\LocalizedException;

class OrderProcessor extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\DB\TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $orderInterface;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $orderSender;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;


    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     * @param \Magento\Sales\Api\Data\OrderInterface $orderInterface
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->transactionFactory = $transactionFactory;
        $this->orderInterface = $orderInterface;
        $this->orderSender = $orderSender;
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->quoteRepository = $quoteRepository;

        parent::__construct($context);
    }

    /**
     * @param array $data
     * @return \Magento\Sales\Model\Order
     * @throws LocalizedException
     * @throws bool
     */
    public function process($data)
    {
        /** @var $order \Magento\Sales\Model\Order */
        $order = $this->orderInterface->loadByIncrementId($data['bfs_orderNo']);

        if (!($data['bfs_debitAuthCode'] == '00')) {
            return $order;
        }

        //Ignore duplicate processing, since we are dealing with full captures
        if ($order->hasInvoices()) {
            return $order;
        }

        if (!$order->getEmailSent()) {
            $this->orderSender->send($order);
        }

        $order->getPayment()->setTransactionId($data['bfs_bfsTxnId']);

        /** @var $invoice \Magento\Sales\Model\Order\Invoice */
        $invoice = $order->prepareInvoice();

        if (!$invoice) {
            throw new LocalizedException(__('Invoice could not be created.'));
        }

        $invoice->register();
        $invoice->capture();

        $order = $invoice->getOrder();
        $payment = $order->getPayment();
        $transaction = $payment->getCreatedTransaction();

        $this->transactionFactory->create()
            ->addObject($payment)
            ->addObject($transaction)
            ->addObject($invoice)
            ->addObject($order)
            ->save();

        $quote = $this->quoteRepository->get($order->getQuoteId());
        $quote
            ->setReservedOrderId($order->getRealOrderId())
            ->setIsActive(false);
        $this->quoteRepository->save($quote);

        return $order;
    }

    /**
     * Cancels the last order, restores the quote
     *
     * @return void
     */
    public function cancelOrderAndRestoreQuote()
    {
        $orderId = $this->checkoutSession->getLastOrderId();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $orderId ? $this->orderFactory->create()->load($orderId) : false;

        if ($order && $order->getId() && $order->getQuoteId() == $this->checkoutSession->getQuoteId()) {
            $order->cancel()->save();
            $quote = $this->quoteRepository->get($order->getQuoteId())->setIsActive(true);
            $this->quoteRepository->save($quote);

            $this->checkoutSession
                ->unsLastQuoteId()
                ->unsLastSuccessQuoteId()
                ->unsLastOrderId()
                ->unsLastRealOrderId();
        }
    }
}
