<?php

namespace CloudBhutan\RMAGateway\Helper;

class ResponseProcessor
{
    /**
     * @var \CloudBhutan\RMAGateway\Model\Nonce\NonceManagement
     */
    protected $nonce;

    /**
     * @var \CloudBhutan\RMAGateway\Model\Token\TokenManagement
     */
    protected $tokenManager;

    /**
     * @var OrderProcessor
     */
    protected $orderProcessor;

    /**
     * @param \CloudBhutan\RMAGateway\Model\Nonce\NonceManagement $nonce
     * @param \CloudBhutan\RMAGateway\Model\Token\TokenManagement $tokenManager
     * @param OrderProcessor $orderProcessor
     */
    public function __construct(
        \CloudBhutan\RMAGateway\Model\Nonce\NonceManagement $nonce,
        \CloudBhutan\RMAGateway\Model\Token\TokenManagement $tokenManager,
        OrderProcessor $orderProcessor
    ) {
        $this->nonce = $nonce;
        $this->tokenManager = $tokenManager;
        $this->orderProcessor = $orderProcessor;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function process($data)
    {
        //$this->nonce->saveNonce($data['nonce']);
        //$this->tokenManager->saveToken($data);
        $this->orderProcessor->process($data);

        return $this;
    }
}