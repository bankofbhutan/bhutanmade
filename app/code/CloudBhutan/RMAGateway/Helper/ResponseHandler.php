<?php

namespace CloudBhutan\RMAGateway\Helper;

class ResponseHandler extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var RMAGatewaySecurity
     */
    protected $rmagatewaySecurity;

    /**
     * @var ResponseProcessor
     */
    protected $responseProcessor;

    /**
     * @var \CloudBhutan\RMAGateway\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param Validator $validator
     * @param ResponseProcessor $responseProcessor
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \CloudBhutan\RMAGateway\Model\Hmac\RMAGatewaySecurity $rmagatewaySecurity,
        \CloudBhutan\RMAGateway\Model\Config $config,
        Validator $validator,
        ResponseProcessor $responseProcessor
    ) {
        $this->validator = $validator;
        $this->config = $config;
        $this->rmagatewaySecurity = $rmagatewaySecurity;
        $this->responseProcessor = $responseProcessor;

        parent::__construct($context);
    }

    /**
     * @param string[string|int| $response
     * @return array|bool
     */
    public function handleResponse($response)
    {

        //$bfs_bfsTxnId = $response['bfs_bfsTxnId'];
        

       // $decryptData = $this->worldlineSecurity->decryptValue($merchantResponse, $this->config->getSecretKey());

      //  $resArray = explode('|',$decryptData);

        // if($resArray[10] == 'S'){
        //     return false;
        // }
        // else if ($resArray[10]=='F'){
        //     $this->responseProcessor->process($resArray);
        //     return true;
        // }



        $validationResult = $this->validator->validate($response);

       // $validationResult = true;


        if ($validationResult === true) {
            $this->responseProcessor->process($response);
        }

        return $validationResult;


        // return $validationResult;
    }
}