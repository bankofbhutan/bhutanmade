<?php

namespace CloudBhutan\BOBWorldline\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class IFrame
 */
class IFrame extends \Magento\Framework\View\Element\Template
{
    /**
     * Get the cancelling url
     *
     * @return string
     */
    public function getCancelUrl()
    {
        return $this->getUrl('bobworldline/checkout/result', ['cancel' => '1']);
    }
}