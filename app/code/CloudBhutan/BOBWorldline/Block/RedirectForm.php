<?php

namespace CloudBhutan\BOBWorldline\Block;


class RedirectForm extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \CloudBhutan\BOBWorldline\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \CloudBhutan\BOBWorldline\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \CloudBhutan\BOBWorldline\Model\Config $config,
        array $data = []
    ) {
        $this->config = $config;

        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isIFrame()
    {
        return $this->config->getFormType() ===  \CloudBhutan\BOBWorldline\Model\Adminhtml\Source\FormType::FORM_TYPE_IFRAME;
    }
}