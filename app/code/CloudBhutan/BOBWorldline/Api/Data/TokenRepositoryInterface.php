<?php

namespace CloudBhutan\BOBWorldline\Api\Data;

/**
 * @api
 */
interface TokenRepositoryInterface
{
    /**
     * Get the token by id.
     *
     * @param int $tokenId
     * @return \CloudBhutan\BOBWorldline\Api\Data\TokenInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException()
     */
    public function getById($tokenId);

    /**
     * Create or update a token.
     *
     * @param \CloudBhutan\BOBWorldline\Api\Data\TokenInterface $token
     * @return \CloudBhutan\BOBWorldline\Api\Data\TokenInterface
     */
    public function save(\CloudBhutan\BOBWorldline\Api\Data\TokenInterface $token);

    /**
     * Get a list of tokens matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface;
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete token
     *
     * @param \CloudBhutan\BOBWorldline\Api\Data\TokenInterface $token
     * @return bool true on success
     */
    public function delete(\CloudBhutan\BOBWorldline\Api\Data\TokenInterface $token);

}