<?php

namespace CloudBhutan\BOBWorldline\Api\Data;


interface NonceRepositoryInterface
{
    /**
     * Create or update a nonce.
     *
     * @param \CloudBhutan\BOBWorldline\Api\Data\NonceInterface $nonce
     * @return \CloudBhutan\BOBWorldline\Api\Data\NonceInterface
     */
    public function save(\CloudBhutan\BOBWorldline\Api\Data\NonceInterface $nonce);

    /**
     * Get a list of nonces matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface;
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}