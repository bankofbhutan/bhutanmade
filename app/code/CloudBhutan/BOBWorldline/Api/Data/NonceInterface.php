<?php

namespace CloudBhutan\BOBWorldline\Api\Data;


interface NonceInterface
{
    const NONCE = 'nonce';
    const CREATED_AT = 'created_at';

    /**
     * @return string
     */
    public function getNonce();

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $nonce
     * @return $this
     */
    public function setNonce($nonce);
}