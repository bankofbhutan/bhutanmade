<?php

namespace CloudBhutan\BOBWorldline\Api\Data;


interface TokenInterface
{
    const ID  = 'id';
    const CUSTOMER_ID  = 'customer_id';
    const CC_TOKEN  = 'cc_token';
    const CC_LAST_FOUR_DIGITS  = 'cc_last_four_digits';
    const CC_MONTH  = 'cc_month';
    const CC_YEAR  = 'cc_year';
    const CC_HOLDER_NAME  = 'cc_holder_name';
    const CC_TYPE  = 'cc_type';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getCustomerId();

    /**
     * @return string
     */
    public function getCcToken();

    /**
     * @return string
     */
    public function getCcLastFourDigits();

    /**
     * @return string
     */
    public function getCcMonth();

    /**
     * @return string
     */
    public function getCcYear();

    /**
     * @return string
     */
    public function getCcHolderName();

    /**
     * @return string
     */
    public function getCcType();

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id);

    /**
     * @param string $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * @param string $token
     * @return $this
     */
    public function setCcToken($token);

    /**
     * @param string $digits
     * @return $this
     */
    public function setCcLastFourDigits($digits);

    /**
     * @param string $month
     * @return $this
     */
    public function setCcMonth($month);

    /**
     * @param string $year
     * @return $this
     */
    public function setCcYear($year);

    /**
     * @param string $name
     * @return $this
     */
    public function setCcHolderName($name);

    /**
     * @param string $type
     * @return $this
     */
    public function setCcType($type);

}