<?php

namespace CloudBhutan\BOBWorldline\Api;

/**
 * Interface for managing guest payment information
 * @api
 */
interface GuestPaymentInformationAdapterInterface
{
    /**
     * @param string $cartId
     * @param string $email
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
     * @return string
     */
    
    public function adaptPlaceOrder(
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    );
}
