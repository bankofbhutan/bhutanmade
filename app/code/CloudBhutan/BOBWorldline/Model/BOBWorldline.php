<?php

namespace CloudBhutan\BOBWorldline\Model;

use Magento\Payment\Model\Method\AbstractMethod;

//TODO refactor - AbstractMethod is deprecated
class BOBWorldline extends AbstractMethod
{
    const CODE = 'bobworldline';
    protected $_code = self::CODE;
    protected $_canCapture = true;
    protected $_isGateway = true;

    /**
     * Check method for processing with base currency
     *
     * @param string $currencyCode
     * @return bool
     */
    public function canUseForCurrency($currencyCode)
    {
        return in_array($currencyCode, array_map('trim',
            explode(',', $this->getConfigData(Config::XML_PATH_CURRENCIES_ALLOWED))));
    }
}