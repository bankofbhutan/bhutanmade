<?php

namespace CloudBhutan\BOBWorldline\Model\Nonce;

use CloudBhutan\BOBWorldline\Api\Data\NonceInterface;

class Nonce extends \Magento\Framework\Model\AbstractModel implements NonceInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\BOBWorldline\Model\ResourceModel\Nonce');
    }

    /**
     * {@inheritDoc}
     */
    public function getNonce()
    {
        return $this->getData(self::NONCE);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setNonce($nonce)
    {
        return $this->setData(self::NONCE, $nonce);
    }

}