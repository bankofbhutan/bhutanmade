<?php

namespace CloudBhutan\BOBWorldline\Model\Nonce;

class NonceManagement
{
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \CloudBhutan\BOBWorldline\Api\Data\NonceRepositoryInterface
     */
    protected $nonceRepository;

    /**
     * @var \CloudBhutan\BOBWorldline\Api\Data\NonceInterfaceFactory
     */
    protected $nonceFactory;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \CloudBhutan\BOBWorldline\Api\Data\NonceRepositoryInterface $nonceRepository
     * @param \CloudBhutan\BOBWorldline\Api\Data\NonceInterfaceFactory $nonceFactory
     */
    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \CloudBhutan\BOBWorldline\Api\Data\NonceRepositoryInterface $nonceRepository,
        \CloudBhutan\BOBWorldline\Api\Data\NonceInterfaceFactory $nonceFactory
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->nonceRepository = $nonceRepository;
        $this->nonceFactory = $nonceFactory;
    }

    /**
     * Generate a unique string
     *
     * @return string
     */
    public function generateNonce()
    {
        return uniqid(true);
    }

    /**
     * @param string $nonce
     * @return $this
     */
    public function saveNonce($nonce)
    {
        if ($this->isNonceUnique($nonce)) {
            /** @var \CloudBhutan\BOBWorldline\Api\Data\NonceInterface $nonceModel */
            $nonceModel = $this->nonceFactory->create();
            $nonceModel->setNonce($nonce);
            $this->nonceRepository->save($nonceModel);
        }

        return $this;
    }

    /**
     * Check if the received nonce is unique against the stored nonces in the database
     *
     * @param string $nonce
     * @param int $age age in seconds
     * @return bool
     */
    public function isNonceUnique($nonce, $age = 0)
    {
        $filter = $this->filterBuilder->setField('nonce')->setValue($nonce)->setConditionType('eq')->create();
        $this->searchCriteriaBuilder->addFilters([$filter]);
        $list = $this->nonceRepository->getList($this->searchCriteriaBuilder->create());

        if ($list->getTotalCount() > 0) {
            return $this->isValid($list->getItems(), $age);
        }

        return true;
    }

    /**
     * Checks if the nonce is recent
     *
     * This check is needed because EveryPay can send the same nonce back twice with a synchronous request
     *
     * @param array $nonce
     * @param int $age age in seconds
     * @return bool
     */
    protected function isValid($nonce, $age = 0)
    {
        if (count($nonce) > 1) {
            //NOTE code should never reach here
            return false;
        }

        return time() - strtotime($nonce[0]['created_at']) <= $age;
    }
}