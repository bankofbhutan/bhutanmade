<?php

namespace CloudBhutan\BOBWorldline\Model;

use Magento\Framework\Exception\LocalizedException;

class PaymentInformationAdapter implements \CloudBhutan\BOBWorldline\Api\PaymentInformationAdapterInterface
{
    /**
     * @var \Magento\Checkout\Api\PaymentInformationManagementInterface
     */
    protected $paymentInformationManagement;

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Form\FormDataBuilder
     */
    protected $formDataBuilder;

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Token\TokenManagement
     */
    protected $tokenManager;

    /**
     * @var \CloudBhutan\BOBWorldline\Logger\Logger
     */
    protected $logger;

    /**
     * @param \Magento\Checkout\Api\PaymentInformationManagementInterface $paymentInformationManagement
     * @param \CloudBhutan\BOBWorldline\Model\Form\FormDataBuilder $formDataBuilder
     * @param \CloudBhutan\BOBWorldline\Model\Token\TokenManagement $tokenManager
     * @param \CloudBhutan\BOBWorldline\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $paymentInformationManagement,
        \CloudBhutan\BOBWorldline\Model\Form\FormDataBuilder $formDataBuilder,
        \CloudBhutan\BOBWorldline\Model\Token\TokenManagement $tokenManager,
        \CloudBhutan\BOBWorldline\Logger\Logger $logger
    ) {
        $this->paymentInformationManagement = $paymentInformationManagement;
        $this->formDataBuilder = $formDataBuilder;
        $this->tokenManager = $tokenManager;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function adaptPlaceOrder(
        $cartId,
        $tokenAgreement,
        $tokenId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        try {
            $orderId = $this->paymentInformationManagement->savePaymentInformationAndPlaceOrder(
                $cartId, $paymentMethod, $billingAddress
            );

            $tokenData = $this->tokenManager->getTokenData($tokenAgreement, $tokenId);
            return json_encode($this->formDataBuilder->getFields($orderId, $tokenData));
        } catch (\Exception $e) {
            $this->logger->error('An error occured while placing the order', [$e->getMessage(), $e->getTrace()]);
            throw new LocalizedException(__($e->getMessage()));
        }
    }
}
