<?php

namespace CloudBhutan\BOBWorldline\Model;

class BOBWorldlineConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepository;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $customerUrl;

    /**
     * @var Token\TokenManagement
     */
    protected $tokenManager;

    /**
     * @var Config
     */
    protected $config;

    /**
     * BOBWorldlineConfigProvider constructor.
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\View\Asset\Repository $assetRepository
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param Token\TokenManagement $tokenManager
     * @param Config $config
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\View\Asset\Repository $assetRepository,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Model\Url $customerUrl,
        \CloudBhutan\BOBWorldline\Model\Token\TokenManagement $tokenManager,
        Config $config
    ) {
        $this->customerSession = $customerSession;
        $this->assetRepository = $assetRepository;
        $this->url = $url;
        $this->customerUrl = $customerUrl;
        $this->tokenManager = $tokenManager;
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getConfig()
    {

        $logos = $this->getLogos();
        return [
            'payment' => [
                'bobworldline' => [
                    'endpoint' => $this->config->getApiEndpoint(),
                    'isIFrame' =>
                        $this->config->getFormType() ===
                        \CloudBhutan\BOBWorldline\Model\Adminhtml\Source\FormType::FORM_TYPE_IFRAME,
                    'customer' => [
                            'cards' => $this->getCards(),
                            'isLoggedIn' => $this->customerSession->isLoggedIn(),
                    ],
                    'customerUrl' => $this->customerUrl->getLoginUrl(),
                    'isTokenEnabled' => $this->config->getCcTokenEnabled(),
                    'deleteUrl' => $this->url->getUrl('bobworldline/token/delete'),
                    'isDisplayLogos' => $this->config->getDisplayLogos(),
                    'logos' => [$logos['visa'], $logos['master_card']], //a hack for ko.js
                    'newCardId' => Config::NEW_CARD_ID
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    protected function getLogos()
    {
        $visa = [
            'image' => $this->assetRepository->getUrl(
                sprintf("CloudBhutan_BOBWorldline::images/visa.png")
            ),
            'type' => 'visa',
        ];

        $mastercard = [
            'image' => $this->assetRepository->getUrl(
                sprintf("CloudBhutan_BOBWorldline::images/master-card.png")
            ),
            'type' => 'master-card',
        ];

        return [
            'visa' => $visa,
            'master_card' => $mastercard
        ];
    }

    /**
     * @return array
     */
    protected function getCards()
    {
        $cards = [];
        $logos = $this->getLogos();
        foreach ($this->tokenManager->getListByCustomerSession() as $token) {
            $cards[] = [
                'id' => $token['id'],
                'label' =>
                    sprintf("**** **** **** %s (%s %s/%s)",
                        $token['cc_last_four_digits'],
                        __('expires'),
                        $token['cc_month'],
                        $token['cc_year']
                    ),
                'image' => $logos[$token['cc_type']]['image'],
                'type' => str_replace('_', '-', $token['cc_type']),
            ];
        }

        if (!empty($cards)) {
            $newCard = [
                'id' => Config::NEW_CARD_ID,
                'label' => 'New card',
                'image' => '',
                'type' => 'new-card'
            ];

            $cards[] = $newCard;
        }

        return $cards;
    }
}