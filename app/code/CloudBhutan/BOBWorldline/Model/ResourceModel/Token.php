<?php

namespace CloudBhutan\BOBWorldline\Model\ResourceModel;

class Token extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('cloudbhutan_bobworldline_token', 'id');
    }
}
