<?php

namespace CloudBhutan\BOBWorldline\Model\ResourceModel\Nonce;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'nonce';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\BOBWorldline\Model\Nonce\Nonce', 'CloudBhutan\BOBWorldline\Model\ResourceModel\Nonce');
    }
}
