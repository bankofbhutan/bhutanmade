<?php

namespace CloudBhutan\BOBWorldline\Model\ResourceModel\Token;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CloudBhutan\BOBWorldline\Model\Token\Token', 'CloudBhutan\BOBWorldline\Model\ResourceModel\Token');
    }
    // @codingStandardsIgnoreEnd
}
