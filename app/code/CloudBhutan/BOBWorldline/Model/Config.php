<?php

namespace CloudBhutan\BOBWorldline\Model;

use CloudBhutan\BOBWorldline\Model\Adminhtml\Source\Environment;

class Config
{
    const XML_PATH_FORM_TYPE = 'form_type';
    const XML_PATH_SKIN_NAME = 'skin_name';
    const XML_PATH_ENVIRONMENT = 'environment';

    // modifications
    const XML_PATH_CURRENCY = 'currency';
    const XML_PATH_MERCHANT_ID_PRODUCTION = 'merchant_id_production';
    const XML_PATH_SECRET_KEY_PRODUCTION = 'secret_key_production';

    const XML_PATH_MERCHANT_ID_TEST = 'merchant_id_test';
    const XML_PATH_SECRET_KEY_TEST = 'secret_key_test';

    const XML_PATH_API_USER_PRODUCTION = 'api_user_production';
    const XML_PATH_API_USER_TEST = 'api_user_test';
    const XML_PATH_API_PASSWORD_PRODUCTION = 'api_password_production';
    const XML_PATH_API_PASSWORD_TEST = 'api_password_test';
    const XML_PATH_PROCESSING_ACCOUNT = 'processing_account';
    const XML_PATH_CC_TOKEN_ENABLED = 'cc_token_enabled';
    const XML_PATH_TOKEN_SECURITY = 'token_security';
    const XML_PATH_TEST_API_ENDPOINT = 'test_api_endpoint';
    const XML_PATH_PRODUCTION_API_ENDPOINT = 'production_api_endpoint';
    const XML_PATH_DEFAULT_LOCALE = 'default_locale';
    const XML_PATH_SUPPORTED_LOCALES = 'supported_locales';
    const XML_PATH_CURRENCIES_ALLOWED = 'currencies_allowed';
    const XML_PATH_DISPLAY_LOGOS = 'display_logos';

    const NEW_CARD_ID = -1;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
    ) {
        $this->storeManager = $storeManagerInterface;
        $this->scopeConfig = $scopeConfigInterface;
    }

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     * @param int|string|null|\Magento\Store\Model\Store $storeId
     *
     * @return mixed
     */
    public function getConfigData($field, $storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        $path = 'payment/bobworldline/' . $field;
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * @return string
     */
    public function getFormType()
    {
        return trim($this->getConfigData(Config::XML_PATH_FORM_TYPE));
    }

    /**
     * @return string
     */
    public function getSkinName()
    {
        $skinName = trim($this->getConfigData(Config::XML_PATH_SKIN_NAME));

        if (empty($skinName)) {
            $skinName = 'default';
        }

        return $skinName;
    }


    // ========================== modifications by tshewang =========================

    
    public function getCurrency()
    {
        return trim($this->getConfigData(Config::XML_PATH_CURRENCY));
    }

    /**
     * @return null|string
     */
    public function getMerchantId()
    {
        $environment = $this->getEnvironment();

        if ($environment == Environment::ENVIRONMENT_PRODUCTION) {
            return $this->getMerchantIdProduction();
        }

        if ($environment == Environment::ENVIRONMENT_TEST) {
            return $this->getMerchantIdTest();
        }

        return null;
    }

     /**
     * @return string
     */
    public function getMerchantIdProduction()
    {
        return trim($this->getConfigData(Config::XML_PATH_MERCHANT_ID_PRODUCTION));
    }

     /**
     * @return string
     */
    public function getMerchantIdTest()
    {
        return trim($this->getConfigData(Config::XML_PATH_MERCHANT_ID_TEST));
    }

    /**
     * @return null|string
     */
    public function getSecretKey()
    {
        $environment = $this->getEnvironment();

        if ($environment == Environment::ENVIRONMENT_PRODUCTION) {
            return $this->getSecretKeyProduction();
        }

        if ($environment == Environment::ENVIRONMENT_TEST) {
            return $this->getSecretKeyTest();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getSecretKeyProduction()
    {
        return trim($this->getConfigData(Config::XML_PATH_SECRET_KEY_PRODUCTION));
    }

    /**
     * @return string
     */
    public function getSecretKeyTest()
    {
        return trim($this->getConfigData(Config::XML_PATH_SECRET_KEY_TEST));
    }



    /**
     * @return null|string
     */
    public function getApiUser()
    {
        $environment = $this->getEnvironment();

        if ($environment == Environment::ENVIRONMENT_PRODUCTION) {
            return $this->getApiUserProduction();
        }

        if ($environment == Environment::ENVIRONMENT_TEST) {
            return $this->getApiUserTest();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getApiPassword()
    {
        $environment = $this->getEnvironment();

        if ($environment == Environment::ENVIRONMENT_PRODUCTION) {
            return $this->getApiPasswordProduction();
        }

        if ($environment == Environment::ENVIRONMENT_TEST) {
            return $this->getApiPasswordTest();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getEnvironment()
    {
        return $this->getConfigData(Config::XML_PATH_ENVIRONMENT);
    }



    /**
     * @return string
     */
    public function getApiUserProduction()
    {
        return trim($this->getConfigData(Config::XML_PATH_API_USER_PRODUCTION));
    }

    /**
     * @return string
     */
    public function getApiUserTest()
    {
        return trim($this->getConfigData(Config::XML_PATH_API_USER_TEST));
    }

    

    /**
     * @return string
     */
    public function getApiPasswordTest()
    {
        return trim($this->getConfigData(Config::XML_PATH_API_PASSWORD_TEST));
    }

    /**
     * @return string
     */
    public function getProcessingAccount()
    {
        return trim($this->getConfigData(Config::XML_PATH_PROCESSING_ACCOUNT));
    }

    /**
     * @return boolean
     */
    public function getCcTokenEnabled()
    {
        return $this->getConfigData(Config::XML_PATH_CC_TOKEN_ENABLED) == 1;
    }

    /**
     * @return string
     */
    public function getTokenSecurity()
    {
        return $this->getConfigData(Config::XML_PATH_TOKEN_SECURITY);
    }

    /**
     * @return string
     */
    public function getTestApiEndpoint()
    {
        return $this->getConfigData(Config::XML_PATH_TEST_API_ENDPOINT);
    }

    /**
     * @return string
     */
    public function getProductionApiEndpoint()
    {
        return $this->getConfigData(Config::XML_PATH_PRODUCTION_API_ENDPOINT);
    }

    /**
     * @return null|string
     */
    public function getApiEndpoint()
    {
        $environment = $this->getEnvironment();

        if ($environment == Environment::ENVIRONMENT_PRODUCTION) {
            return $this->getProductionApiEndpoint();
        }

        if ($environment == Environment::ENVIRONMENT_TEST) {
            return $this->getTestApiEndpoint();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this->getConfigData(Config::XML_PATH_DEFAULT_LOCALE);
    }

    /**
     * @return array
     */
    public function getSupportedLocales()
    {
        return explode(',', $this->getConfigData(Config::XML_PATH_SUPPORTED_LOCALES));
    }

    /**
     * @return array
     */
    public function getAllowedCurrencies()
    {
        return array_map('trim', explode(',', $this->getConfigData('currencies_allowed')));
    }

    /**
     * @return bool
     */
    public function getDisplayLogos()
    {
        return $this->getConfigData(Config::XML_PATH_DISPLAY_LOGOS) == 1;
    }
}