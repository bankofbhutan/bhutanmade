<?php

namespace CloudBhutan\BOBWorldline\Model\Hmac;

class HmacManagement
{
    const HMAC_FIELDS = 'hmac_fields';
    const HMAC = 'hmac';

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Config
     */
    protected $config;

    /**
     * @param \CloudBhutan\BOBWorldline\Model\Config $config
     */
    public function __construct(
        \CloudBhutan\BOBWorldline\Model\Config $config
    ) {
        $this->config = $config;
    }

    /**
     * Appends the 'hmac_fields' and 'hmac' values to the form data array
     *
     * @param array $data
     * @return array
     */
    public function appendHmacFields(array $data)
    {
        $data[self::HMAC_FIELDS] = $this->getHmacFields($data);
        $data[self::HMAC] = $this->getHmac($data);

        return $data;
    }

    /**
     * @param array $data
     * @return string
     */
    public function getHmacFields(array $data)
    {
        $keys = array_keys($data);

        if (!in_array(self::HMAC_FIELDS, $keys)) {
            $keys[] = self::HMAC_FIELDS;
        }

        asort($keys);

        return implode(',', $keys);
    }

    /**
     * @param array $data
     * @return string
     */
    public function getHmac(array $data)
    {
        return $this->signData($this->serializeData($data));
    }

    /**
     * @param array $data
     * @return string
     */
    protected function serializeData(array $data)
    {
        $serialized = [];
        ksort($data);

        foreach ($data as $key => $value) {
            $serialized[] = $key . '=' . $value;
        }

        return implode('&', $serialized);
    }

    /**
     * @param string $data
     * @return string
     */
    protected function signData($data)
    {
        return hash_hmac('sha1', $data, $this->config->getApiPassword());
    }

    /**
     * @param array $data
     * @return string
     */
    public function calculateResponseHmac($data)
    {
        $hmacFields = explode(',', $data[self::HMAC_FIELDS]);
        $verify = [];

        foreach ($hmacFields as $field) {
            $verify[$field] = isset($data[$field]) ? $data[$field] : '';
        }

        return $this->getHmac($verify);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function isResponseHmacValid($data)
    {
        if (isset($data[self::HMAC])) {
            return $this->calculateResponseHmac($data) === $data[self::HMAC];
        }
        return false;
    }

}