<?php

namespace CloudBhutan\BOBWorldline\Model\Form;

use Magento\Framework\Exception\LocalizedException;
use CloudBhutan\BOBWorldline\Model\Hmac\WorldlineSecurity;

class WorldlineData extends AbstractData
{
    
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;


    /**
     * @var \Magento\Directory\Api\CountryInformationAcquirerInterface
     */
    protected $country;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Hmac\WorldlineSecurity
     */
    protected $worldlineSecurity;

    /**
     * @var \CloudBhutan\BOBWorldline\Logger\Logger
     */
    protected $logger;

    /**
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Directory\Api\CountryInformationAcquirerInterface $country
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \CloudBhutan\BOBWorldline\Model\Config $config
     * @param \CloudBhutan\BOBWorldline\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Directory\Api\CountryInformationAcquirerInterface $country,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \CloudBhutan\BOBWorldline\Model\Config $config,
        \CloudBhutan\BOBWorldline\Logger\Logger $logger,
        \CloudBhutan\BOBWorldline\Model\Hmac\WorldlineSecurity $worldlineSecurity
    ) {
        $this->country = $country;
        $this->remoteAddress = $remoteAddress;
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->worldlineSecurity = $worldlineSecurity;
        $this->urlBuilder = $urlBuilder;

        parent::__construct($orderRepository, $config);
    }

    /**
     * @param int $orderId
     * @return array
     * @throws LocalizedException
     */
    public function getFields($orderId)
    {

        $merchantRequest = $this->worldlineSecurity->encryptValue($this->getWorldlineFields($orderId), $this->config->getSecretKey());
        $data = [
            'merchantRequest' => $merchantRequest,
            'MID' => $this->config->getMerchantId()
        ];

        foreach ($data as $field => $value) {
            if (empty($value)) {
                throw new LocalizedException(__('Required field %1 is missing', $field));
            }
        }
        return $data;
    }

    /** worldine fields
    *
    */
    public function getWorldlineFields($orderId){

        $order = $this->orderRepository->get($orderId);

        
        $mId = $this->config->getMerchantId();

        $trnAmt = number_format($order->getBaseGrandTotal() * 100, 0, '.', '');
        $orderId = $order->getRealOrderId();
        $trnCurrency = $this->config->getCurrency();
        $trnRemarks =  '' ;
        $meTransReqType = 'S';
        $recurrPeriod = '';
        $recurrDay = '';
        $noOfRecurring = '';
        $responseUrl = $this->urlBuilder->getRouteUrl('bobworldline/checkout/result');
        $addField1 = '';
        $addField2 = '';
        $addField3 = '';
        $addField4 = '';
        $addField5 = '';
        $addField6 = '';
        $addField7 = '';
        $addField8 = '';
        $addField9 = 'NA';
        $addField10 = 'NA';
        
        return $mId  . "|" . $orderId  . "|" . $trnAmt  . "|" . 
            $trnCurrency  . "|" . $trnRemarks  . "|" . $meTransReqType  . "|" . 
            $recurrPeriod  . "|" . $recurrDay  . "|" . $noOfRecurring  . "|" . 
            $responseUrl  . "|" . $addField1  . "|" . $addField2  . "|" . 
            $addField3  . "|" . $addField4  . "|" . $addField5  . "|" . 
            $addField6  . "|" . $addField7  . "|" . $addField8  . "|" . 
            $addField9  . "|" . $addField10 ;

    }

    /**
     * Remove single quotes from field values to get a correct HMAC
     *
     * @param array $fields
     * @return mixed
     */
    protected function sanitizeFields($fields) {
        foreach ($fields as $field => $value) {
            $fields[$field] = str_replace("'", "", $value);
        }

        return $fields;
    }


    /**
     *
     * @param
     *          $inputVal
     * @param
     *          $secureKey
     * @return string
     */
    protected function encryptValue($inputVal, $secureKey) {
        $key = '';
        for($i = 0; $i < strlen ( $secureKey ) - 1; $i += 2) {
            $key .= chr ( hexdec ( $secureKey [$i] . $secureKey [$i + 1] ) );
        }
        
        $block = @mcrypt_get_block_size ( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB );
        $pad = $block - (strlen ( $inputVal ) % $block);
        $inputVal .= str_repeat ( chr ( $pad ), $pad );
        
        $encrypted_text = bin2hex ( @mcrypt_encrypt ( MCRYPT_RIJNDAEL_128, $key, $inputVal, MCRYPT_MODE_ECB ) );
        
        return $encrypted_text;
    }
    /**
     *
     * @param
     *          $inputVal
     * @param
     *          $secureKey
     * @return string
     */
    protected function decryptValue($inputVal, $secureKey) {
        $key = '';
        for($i = 0; $i < strlen ( $secureKey ) - 1; $i += 2) {
            
            $key .= chr ( hexdec ( $secureKey [$i] . $secureKey [$i + 1] ) );
        }
        
        $encblock = '';
        for($i = 0; $i < strlen ( $inputVal ) - 1; $i += 2) {
            $encblock .= chr ( hexdec ( $inputVal [$i] . $inputVal [$i + 1] ) );
        }
        
        $decrypted_text = @mcrypt_decrypt ( MCRYPT_RIJNDAEL_128, $key, $encblock, MCRYPT_MODE_ECB );
        
        return $decrypted_text;
    }
}
