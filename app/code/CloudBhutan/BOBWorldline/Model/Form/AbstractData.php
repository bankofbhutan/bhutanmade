<?php

namespace CloudBhutan\BOBWorldline\Model\Form;

abstract class AbstractData
{
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \CloudBhutan\BOBWorldline\Model\Config $config
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \CloudBhutan\BOBWorldline\Model\Config $config
    ) {
        $this->orderRepository = $orderRepository;
        $this->config = $config;
    }

    /**
     * @param int $orderID
     * @return mixed
     */
    public abstract function getFields($orderID);
}