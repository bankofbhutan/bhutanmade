<?php

namespace CloudBhutan\BOBWorldline\Model\Token;

class TokenManagement
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \CloudBhutan\BOBWorldline\Api\Data\TokenInterfaceFactory
     */
    protected $tokenFactory;

    /**
     * @var \CloudBhutan\BOBWorldline\Api\Data\TokenRepositoryInterface
     */
    protected $tokenRepository;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $order;

    /**
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \CloudBhutan\BOBWorldline\Api\Data\TokenInterfaceFactory $tokenFactory
     * @param \CloudBhutan\BOBWorldline\Api\Data\TokenRepositoryInterface $tokenRepository
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \CloudBhutan\BOBWorldline\Api\Data\TokenInterfaceFactory $tokenFactory,
        \CloudBhutan\BOBWorldline\Api\Data\TokenRepositoryInterface $tokenRepository,
        \Magento\Sales\Api\Data\OrderInterface $order
    ) {
        $this->customerSession = $customerSession;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->tokenFactory = $tokenFactory;
        $this->tokenRepository = $tokenRepository;
        $this->order = $order;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function saveToken($data)
    {
        if (!isset($data['cc_token'])) {
            return $this;
        }

        /** @var $order \Magento\Sales\Model\Order */
        try {
            $order = $this->order->loadByIncrementId($data['order_reference']);
        } catch (\Exception $e) {
            return $this;
        }

        if (!$order->getCustomerId()) {
            return $this;
        }

        $customerFilter = $this->getCustomerFilter($order);
        $tokenFilter = $this->getTokenFilter($data['cc_token']);
        $this->searchCriteriaBuilder->addFilters([$customerFilter]);
        $this->searchCriteriaBuilder->addFilters([$tokenFilter]);
        $count = $this->tokenRepository->getList($this->searchCriteriaBuilder->create())->getTotalCount();

        if ($count == 0) {
            /** @var \CloudBhutan\BOBWorldline\Api\Data\TokenInterface $tokenModel */
            $tokenModel = $this->tokenFactory->create();

            $tokenModel
                ->setCustomerId($order->getCustomerId())
                ->setCcToken($data['cc_token'])
                ->setCcLastFourDigits($data['cc_last_four_digits'])
                ->setCcMonth($data['cc_month'])
                ->setCcYear($data['cc_year'])
                ->setCcHolderName($data['cc_holder_name'])
                ->setCcType($data['cc_type']);

            $this->tokenRepository->save($tokenModel);
        }

        return $this;
    }

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getListByCustomerSession()
    {
        $this->searchCriteriaBuilder->addFilters([$this->getCustomerFilter()]);
        return $this->tokenRepository->getList($this->searchCriteriaBuilder->create())->getItems();
    }

    /**
     * @param \Magento\Sales\Model\Order|null $order
     * @return \Magento\Framework\Api\Filter
     */
    protected function getCustomerFilter($order = null)
    {
        if ($order) {
            $customerId = $order->getCustomerId();
        } else {
            $customerId = $this->customerSession->getId();
        }

        return $this->filterBuilder
            ->setField('customer_id')
            ->setValue($customerId)
            ->setConditionType('eq')
            ->create();
    }

    /**
     * @param string $token
     * @return \Magento\Framework\Api\Filter
     */
    protected function getTokenFilter($token)
    {
        return $this->filterBuilder
            ->setField('cc_token')
            ->setValue($token)
            ->setConditionType('eq')
            ->create();
    }

    /**
     * Security check whether the token belongs to the current customer
     *
     * @param \CloudBhutan\BOBWorldline\Api\Data\TokenInterface $token
     * @return bool
     */
    public function isCustomersToken(\CloudBhutan\BOBWorldline\Api\Data\TokenInterface $token)
    {
        return $token->getCustomerId() == $this->customerSession->getId();
    }

    /**
     * @param int|bool $isAgree
     * @param int $tokenId
     * @return array|null
     */
    public function getTokenData($isAgree, $tokenId)
    {
        if ($isAgree) {
            $tokenData = [
                'request_cc_token' => $isAgree,
                'token_id' => $tokenId
            ];
        } else {
            $tokenData = null;
        }

        return $tokenData;
    }
}