<?php

namespace CloudBhutan\BOBWorldline\Helper;

class ResponseProcessor
{
    /**
     * @var \CloudBhutan\BOBWorldline\Model\Nonce\NonceManagement
     */
    protected $nonce;

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Token\TokenManagement
     */
    protected $tokenManager;

    /**
     * @var OrderProcessor
     */
    protected $orderProcessor;

    /**
     * @param \CloudBhutan\BOBWorldline\Model\Nonce\NonceManagement $nonce
     * @param \CloudBhutan\BOBWorldline\Model\Token\TokenManagement $tokenManager
     * @param OrderProcessor $orderProcessor
     */
    public function __construct(
        \CloudBhutan\BOBWorldline\Model\Nonce\NonceManagement $nonce,
        \CloudBhutan\BOBWorldline\Model\Token\TokenManagement $tokenManager,
        OrderProcessor $orderProcessor
    ) {
        $this->nonce = $nonce;
        $this->tokenManager = $tokenManager;
        $this->orderProcessor = $orderProcessor;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function process($data)
    {
        //$this->nonce->saveNonce($data['nonce']);
        //$this->tokenManager->saveToken($data);
        $this->orderProcessor->process($data);

        return $this;
    }
}