<?php

namespace CloudBhutan\BOBWorldline\Helper;

class ResponseHandler extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var WorldlineSecurity
     */
    protected $worldlineSecurity;

    /**
     * @var ResponseProcessor
     */
    protected $responseProcessor;

    /**
     * @var \CloudBhutan\BOBWorldline\Model\Config
     */
    protected $config;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param Validator $validator
     * @param ResponseProcessor $responseProcessor
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \CloudBhutan\BOBWorldline\Model\Hmac\WorldlineSecurity $worldlineSecurity,
        \CloudBhutan\BOBWorldline\Model\Config $config,
        Validator $validator,
        ResponseProcessor $responseProcessor
    ) {
        $this->validator = $validator;
        $this->config = $config;
        $this->worldlineSecurity = $worldlineSecurity;
        $this->responseProcessor = $responseProcessor;

        parent::__construct($context);
    }

    /**
     * @param string[string|int| $response
     * @return array|bool
     */
    public function handleResponse($response)
    {

        $merchantResponse = $response['merchantResponse'];

        $decryptData = $this->worldlineSecurity->decryptValue($merchantResponse, $this->config->getSecretKey());

        $resArray = explode('|',$decryptData);

        // if($resArray[10] == 'S'){
        //     return false;
        // }
        // else if ($resArray[10]=='F'){
        //     $this->responseProcessor->process($resArray);
        //     return true;
        // }



        $validationResult = $this->validator->validate($resArray);

        if ($validationResult === true) {
            $this->responseProcessor->process($resArray);
        }

        return $validationResult;

        // $validationResult = $this->validator->validate($response);

        // if ($validationResult === true) {
        //     
        // }

        // return $validationResult;

        //return true;
    }
}