<?php

namespace CloudBhutan\BOBWorldline\Helper;

use Magento\Framework\Exception\LocalizedException;

class OrderProcessor extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\DB\TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $orderInterface;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $orderSender;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;


    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     * @param \Magento\Sales\Api\Data\OrderInterface $orderInterface
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->transactionFactory = $transactionFactory;
        $this->orderInterface = $orderInterface;
        $this->orderSender = $orderSender;
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->quoteRepository = $quoteRepository;

        parent::__construct($context);
    }

    /**
     * @param array $data
     * @return \Magento\Sales\Model\Order
     * @throws LocalizedException
     * @throws bool
     */
    public function process($data)
    {
        /** @var $order \Magento\Sales\Model\Order */
        $order = $this->orderInterface->loadByIncrementId($data[1]);


        /*
        $resMsgDTO->setPgMeTrnRefNo($strings[0]); 
                $resMsgDTO->setOrderId($strings[1]) ;      // ORDERID
                $resMsgDTO->setTrnAmt($strings[2]) ; 
                $resMsgDTO->setAuthNStatus($strings[3]);  // AUTHNSTATUS
                $resMsgDTO->setAuthZStatus($strings[4]);
                $resMsgDTO->setCaptureStatus($strings[5]); 
                $resMsgDTO->setRrn($strings[6]);
                $resMsgDTO->setAuthZCode($strings[7]);
                $resMsgDTO->setResponseCode($strings[8]);
                $resMsgDTO->setTrnReqDate($strings[9]);
                $resMsgDTO->setStatusCode($strings[10]);
                $resMsgDTO->setStatusDesc($strings[11]);
                $resMsgDTO->setAddField1($strings[12]);
                $resMsgDTO->setAddField2($strings[13]);
                $resMsgDTO->setAddField3($strings[14]);
                $resMsgDTO->setAddField4($strings[15]);
                $resMsgDTO->setAddField5($strings[16]);
                $resMsgDTO->setAddField6($strings[17]);
                $resMsgDTO->setAddField7($strings[18]);
                $resMsgDTO->setAddField8($strings[19]);
                $resMsgDTO->setAddField9($strings[20]);
                $resMsgDTO->setAddField10($strings[21]); 
        */

        if (!($data[10] == 'S')) {
            return $order;
        }

        //Ignore duplicate processing, since we are dealing with full captures
        if ($order->hasInvoices()) {
            return $order;
        }

        if (!$order->getEmailSent()) {
            $this->orderSender->send($order);
        }

        $order->getPayment()->setTransactionId($data[0]);

        /** @var $invoice \Magento\Sales\Model\Order\Invoice */
        $invoice = $order->prepareInvoice();

        if (!$invoice) {
            throw new LocalizedException(__('Invoice could not be created.'));
        }

        $invoice->register();
        $invoice->capture();

        $order = $invoice->getOrder();
        $payment = $order->getPayment();
        $transaction = $payment->getCreatedTransaction();

        $this->transactionFactory->create()
            ->addObject($payment)
            ->addObject($transaction)
            ->addObject($invoice)
            ->addObject($order)
            ->save();

        $quote = $this->quoteRepository->get($order->getQuoteId());
        $quote
            ->setReservedOrderId($order->getRealOrderId())
            ->setIsActive(false);
        $this->quoteRepository->save($quote);

        return $order;
    }

    /**
     * Cancels the last order, restores the quote
     *
     * @return void
     */
    public function cancelOrderAndRestoreQuote()
    {
        $orderId = $this->checkoutSession->getLastOrderId();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $orderId ? $this->orderFactory->create()->load($orderId) : false;

        if ($order && $order->getId() && $order->getQuoteId() == $this->checkoutSession->getQuoteId()) {
            $order->cancel()->save();
            $quote = $this->quoteRepository->get($order->getQuoteId())->setIsActive(true);
            $this->quoteRepository->save($quote);

            $this->checkoutSession
                ->unsLastQuoteId()
                ->unsLastSuccessQuoteId()
                ->unsLastOrderId()
                ->unsLastRealOrderId();
        }
    }
}
