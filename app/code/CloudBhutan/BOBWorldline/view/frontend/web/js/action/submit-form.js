define(
    [
        'jquery',
        'Magento_Checkout/js/model/full-screen-loader'
    ],
    function ($, fullScreenLoader) {
        'use strict';

        function createForm(data) {
            var endpoint = window.checkoutConfig.payment.bobworldline.endpoint;

            var form = "<form id='bobworldline-transaction-data-form'  action='" + endpoint + "' method='post'";
            if (window.checkoutConfig.payment.bobworldline.isIFrame) {
                form += "target='bobworldline-payment-container'";
            }
            form += ">";

            $.each(data, function(name, value) {
                form += "<input type='text' name='" + name + "' value='" + value + "' hidden />";
            });
            form += "<input type='submit' class='bobworldline-submit-button' value='Submit' hidden /></form>";

            return form;
        }

        return function (data) {
            $("#bobworldline-transaction-data-form").remove();
            var form = createForm(data);
            $('body').append(form);
            $(".bobworldline-submit-button").click();

            fullScreenLoader.stopLoader();
            if (window.checkoutConfig.payment.bobworldline.isIFrame) {
                $('#bobworldline-popup').modal('openModal');
            }
            return true;
        };
    }
);
