define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'bobworldline',
                component: 'CloudBhutan_BOBWorldline/js/view/payment/method-renderer/bobworldline'
            }
        );

        return Component.extend({});
    }
);